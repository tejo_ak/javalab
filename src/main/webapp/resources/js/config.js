CKEDITOR.editorConfig = function (config) {
    config.extraPlugins = 'uicolor';
    config.resize_enabled = false;
    config.toolbar = 'MyToolbar';
    config.toolbar_MyToolbar = [
        [ 'Save', 'Bold', 'Italic', 'Underline' , 'Subscript',
            'Superscript', 'TextColor', 'BGColor', '-', 'Cut', 'Copy',
            'Paste', 'Link', 'Unlink' ],
        [ 'Undo', 'Redo', '-', 'JustifyLeft', 'JustifyCenter',
            'JustifyRight', 'JustifyBlock', 'Indent', 'Outdent' ],
        [   'Format', 'Font', 'FontSize', 'Maximize' ]
    ];
};