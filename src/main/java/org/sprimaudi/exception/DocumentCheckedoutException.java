package org.sprimaudi.exception;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 8/24/12
 * Time: 10:51 AM
 * To change this template use File | Settings | File Templates.
 */
public class DocumentCheckedoutException extends RuntimeException {
    public DocumentCheckedoutException() {
    }

    public DocumentCheckedoutException(String message) {
        super(message);
    }

    public DocumentCheckedoutException(String message, Throwable cause) {
        super(message, cause);
    }

    public DocumentCheckedoutException(Throwable cause) {
        super(cause);
    }
}
