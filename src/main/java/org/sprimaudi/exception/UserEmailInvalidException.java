package org.sprimaudi.exception;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 2/19/13
 * Time: 9:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class UserEmailInvalidException extends RuntimeException {
    public UserEmailInvalidException() {
    }

    public UserEmailInvalidException(String message) {
        super(message);
    }

    public UserEmailInvalidException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserEmailInvalidException(Throwable cause) {
        super(cause);
    }
}
