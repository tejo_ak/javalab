package org.sprimaudi.exception;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 8/24/12
 * Time: 10:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class NonWorkflowDocumentException extends RuntimeException {
    public NonWorkflowDocumentException() {
    }

    public NonWorkflowDocumentException(String message) {
        super(message);
    }

    public NonWorkflowDocumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public NonWorkflowDocumentException(Throwable cause) {
        super(cause);
    }
}
