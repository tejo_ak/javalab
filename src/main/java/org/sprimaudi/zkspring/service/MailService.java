package org.sprimaudi.zkspring.service;

import org.sprimaudi.zkutil.ContextMgt;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.zkoss.zk.ui.event.Events;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 9/20/12
 * Time: 9:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Service(value = "mailService")
public class MailService {
    @Inject
    private MailSender mailSender;

    public static final String NOREPLY_ORIGIN = "noreply@signal.org";

    @Inject
    ContextMgt ctx;

    public final static String MAIL_SERVICE_PROP = "mail_service";
    public final static String MAIL_SERVICE_PROP_ACTIVE = "active";
    public final static String MAIL_SERVICE_PROP_PASSIVE = "passive";

    public void sendMail(String from, String to, String subject, String body) {
        String mailActive = ctx.readProperty(MAIL_SERVICE_PROP, "active");
        if (MAIL_SERVICE_PROP_ACTIVE.equals(mailActive)) {
            SimpleMailMessage message = new SimpleMailMessage();

            message.setFrom(from);
            message.setTo(to);
            message.setSubject(subject);
            message.setText(body);

            mailSender.send(message);
        }
    }

}
