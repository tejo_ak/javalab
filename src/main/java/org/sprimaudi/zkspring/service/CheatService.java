package org.sprimaudi.zkspring.service;

import com.djbc.utilities.StringUtil;
import org.sprimaudi.zkutil.ContextMgt;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: lenovo
 * Date: 2/17/13
 * Time: 2:14 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("cheatService")
public class CheatService {
    public static final String cheatPrefix = "#$";

    public boolean isCheat(String cheat) {
        if (cheat != null && cheat.startsWith(cheatPrefix)) {
            return true;
        }
        return false;
    }

    public String cleanCheat(String cheat) {
        if (isCheat(cheat)) {
            return StringUtil.right(cheat, cheat.length() - cheatPrefix.length());
        }
        return cheat;
    }

    @Inject
    ReferensiService referensiService;
    @Inject
    ContextMgt ctx;

    public void cheat(String cheat) {
        cheat = cleanCheat(cheat);
        if ("reinitapp".equals(cheat)) {
            appInit();
        } else if (cheat.startsWith("enablemailservice")) {
            if (cheat.endsWith("off")) {
                ctx.readProperty(MailService.MAIL_SERVICE_PROP, MailService.MAIL_SERVICE_PROP_PASSIVE, true);
            } else if (cheat.endsWith("on")) {
                ctx.readProperty(MailService.MAIL_SERVICE_PROP, MailService.MAIL_SERVICE_PROP_ACTIVE, true);
            }
        }
    }

    @Inject
    AppInitService appInitService;

    private void appInit() {
        appInitService.reInitAll();
    }
}
