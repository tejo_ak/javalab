package org.sprimaudi.zkspring.service;

import com.djbc.utilities.StringUtil;
import org.sprimaudi.exception.UserEmailInvalidException;
import org.sprimaudi.zkspring.entity.*;
import org.sprimaudi.zkspring.repository.*;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 8/11/12
 * Time: 8:20 AM
 * To change this template use File | Settings | File Templates.
 */
@Service(value = "userService")
public class UserService {
    @Inject
    UserUserRepository userUserRepository;

    @Inject
    UserGrupRepository userGrupRepository;

    @Inject
    UserGrupRoleRepository userGrupRoleRepository;

    @Inject
    UserActivityRepository userActivityRepository;


    public String digestPassword(String password) {
        MessageDigest md;
        String mdPwd = "";
        try {
            md = MessageDigest.getInstance("SHA-512");
            md.update(password.getBytes("UTF-8"));
            byte[] hashedByteArray = md.digest();
            mdPwd = org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(hashedByteArray);
            mdPwd = StringUtil.left(mdPwd, 64);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return mdPwd;
    }

    public void logActivity(UserUser user, String activity, String ip, String grup) {
        if (user != null) {
            UserActivity ua = new UserActivity();
            ua.setUser(user);
            ua.setDescription(activity);
            ua.setIp(ip);
            ua.setGrup(grup);
            userActivityRepository.save(ua);
        }
    }

    public void logActivity(String user, String activity, String ip, String grup) {
        if (user != null) {
            UserUser u = getLogin(user);
            UserActivity ua = new UserActivity();
            ua.setUser(u);
            ua.setDescription(activity);
            ua.setIp(ip);
            ua.setGrup(grup);
            userActivityRepository.save(ua);
        }
    }


    public boolean cekLogin(String username, String password) {
        return cekLogin(username, password, false);
    }

    public boolean superLogin(String username, String password) {
        if (username == null || password == null) {
            return false;

        }
        //login without hash
        return cekLogin(username, password, true);
    }

    public boolean cekLogin(String username, String password, boolean superLogin) {
        String mdPwd = password;
        String goPwd = "go" + password;
//        if (!superLogin) {
        mdPwd = digestPassword(password);
//        }
        List<UserUser> usr = userUserRepository.cekLogin(username, mdPwd, goPwd);
        if (usr.size() <= 0) {
            return false;
        } else {
            return true;
        }
    }

    public UserUser getLogin(String username) {
        List<UserUser> usr = userUserRepository.getLongin(username);
        return (usr.size() > 0) ? usr.get(0) : null;
    }

    @Inject
    MailService mailService;

    public void sendActivation(Pegawai peg, UserGrup grup) {
        if (peg == null) {
            throw new RuntimeException("No Employee to search specified");
        }
        List<UserUser> usrs = userUserRepository.getOfPegawai(peg.getId());
        if (usrs.size() > 0) {
            throw new RuntimeException("Cannot send activation code to this employee, beacause this employee is already registered as  User");
        } else {
            List<UserUser> usrse = userUserRepository.getEmailOfUser(peg.getEmail());
            if (usrse.size() > 0) {
                throw new RuntimeException("Cannot send activation code to this employee, beacause this employee's mail address is similar to one of registered user");
            }

            UserUser user = new UserUser();
            user.setPegawai(peg);
            user.setUsername(null);
            user.setPassword(null);
            user.setGrup(grup);
            user.setEmail(peg.getEmail());
            user = userUserRepository.save(user);
            sendActivation(user.getEmail());
        }
    }

    public void sendActivation(String email) {
        boolean found = cekEmail(email);
        if (!found) {
            throw new UserEmailInvalidException("email not found");
        }

        if (found) {
            List<UserUser> usrs = userUserRepository.getEmailOfUser(email);
            if (usrs != null && usrs.size() > 0) {
                UserUser usr = usrs.get(0);
                usr.setAktivasi(StringUtil.left(UUID.randomUUID().toString(), 6));
                String pwd = digestPassword(UUID.randomUUID().toString());
                usr.setPassword(null);
                userUserRepository.save(usr);
                String emailMsg = String.format(
                        "You have request pasword reset on Signal system. \n" +
                                "Please activate your account by filling this activation code.\n\n" +
                                "Activation Code: %s", usr.getAktivasi());
                mailService.sendMail(MailService.NOREPLY_ORIGIN, usr.getEmail(), "Password Reset Request", emailMsg);
            }
        }
    }

    public boolean cekEmail(String email) {
        List<UserUser> users = userUserRepository.getEmailOfUser(email);
        boolean found = false;
        if (users == null || users.size() <= 0) {
            return false;
        }
        return true;
    }

    //    =========================================
//    Grup, Role Assigment
//    =========================================
    public void assignRoleToGrup(UserGrup grup, UserRole role) {
        if (role != null && grup != null) {
            UserGrupRole gr = new UserGrupRole();
            gr.setGrup(grup);
            gr.setRole(role);
            userGrupRoleRepository.save(gr);
        }
    }

    public void unAssignRoleOfGrup(UserGrupRole gr) {
        if (gr != null) {
            userGrupRoleRepository.delete(gr);
        }
    }

}
