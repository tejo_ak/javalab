package org.sprimaudi.zkspring.service;

import org.sprimaudi.zkspring.entity.*;
import org.sprimaudi.zkspring.repository.ReferensiRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import utilities.StringUtil;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 7/21/12
 * Time: 4:25 PM
 * To change this template use File | Settings | File Templates.
 */
@Service(value = "referensiService")
public class ReferensiService {
    private final static Long GRUP_INCREMENT = 2L;

    @Inject
    ReferensiRepository referensiRepository;




    public Referensi simpan(Referensi ref) {
        if (ref == null) {
            return null;
        }
        return referensiRepository.save(ref);
    }

    public Referensi byKodeGrup(String kode, Long grup) {
        return referensiRepository.byKodeGrup(kode, grup);
    }


    //    @Transactional(isolation = Isolation.SERIALIZABLE, propagation = Propagation.REQUIRED,readOnly = false)
    public Long increment(String kode) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int tahunIni = cal.get(Calendar.YEAR);
        Referensi r = referensiRepository.byGrupAndKode(GRUP_INCREMENT, kode);
        Long cur = r.getNum();
        Long next = cur + 1;
        r.setNum(next);
        Long tahunReferensi = r.getNilai();
        if (tahunIni > tahunReferensi) {
            r.setNilai(tahunReferensi++);
        }
        referensiRepository.save(r);
        return next;
    }

}
