package org.sprimaudi.zkspring.service;

import org.sprimaudi.zkspring.entity.*;
import org.sprimaudi.zkspring.repository.*;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 2/11/13
 * Time: 3:53 PM
 * To change this template use File | Settings | File Templates.
 */
@Service("appInitService")
public class AppInitService {

    @Inject
    UnitRepository unitRepository;
    @Inject
    UserActivityRepository userActivityRepository;
    @Inject
    UserGrupRepository userGrupRepository;
    @Inject
    UserGrupRoleRepository userGrupRoleRepository;
    @Inject
    UserRoleRepository userRoleRepository;
    @Inject
    UserUserRepository userUserRepository;
    @Inject
    WorkflowRepository workflowRepository;
    @Inject
    WorkflowStatusRepository workflowStatusRepository;
    @Inject
    PegawaiRepository pegawaiRepository;
    @Inject
    ReferensiRepository referensiRepository;


    public void delAll() {

        workflowRepository.deleteAll();
        workflowStatusRepository.deleteAll();
        userGrupRoleRepository.deleteAll();
        userUserRepository.deleteAll();
        userRoleRepository.deleteAll();
        userGrupRepository.deleteAll();
        pegawaiRepository.deleteAll();
        unitRepository.deleteAll();
        userUserRepository.deleteAll();
        referensiRepository.deleteAll();
    }

    public void createUnit(String nama, String alamat, String kode) {
        Unit u = null;
        u = new Unit();
        u.setAlamat(alamat);
        u.setNama(nama);
        u.setKode(kode);
        unitRepository.save(u);
    }

    public void initUnit() {
        createUnit("Waskon", "Waskon", "JKT");
        createUnit("PDI", "Aceh", "ACH");
    }

    public void createPegawai(String nama, String nip, String email, String kdJab, String kdUnit) {
        Referensi jab = referensiRepository.byGrupAndKode(8L, kdJab);
        Unit unit = unitRepository.getByKode(kdUnit);
        Pegawai peg = new Pegawai();
        peg.setNama(nama);
        peg.setEmail(email);
        peg.setEmail(email);
        peg.setUnit(unit);
        peg.setJabatan(jab);
        peg.setNip(nip);
        pegawaiRepository.save(peg);

    }

    public void initPegawai() {
        createPegawai("TEJO AK", "060114143", "tejo.ak@gmail.com", "CSV", "SBY");
        createPegawai("ERWIN DARMIN HERMAWAN", "060114144", "erwindombos@yahoo.com", "KAC", "JGJ");
        createPegawai("BASUKI ADRIANUS SUKOCO", "060114145", "basuki@yahoo.com", "TLL", "PDG");
        createPegawai("RINI MARIANI DEVI", "060114146", "rini@yahoo.com", "KAC", "SBY");
        createPegawai("DEWI PERTIWI NINGSIH", "060114147", "raisa@gmail.com", "TLL", "MDN");
        createPegawai("GUNAWAN TRIWICAKSONO", "060114148", "gunawan@yahoo.com", "CSV", "ACH");
        createPegawai("HADI PURYADIANTO", "060114149", "hadi@hotmail.com", "KAC", "PDG");
        createPegawai("DINA MELATI SUKMA", "060114146", "dina@hotmail.com", "TLL", "SMG");
        createPegawai("LINDA GUNAWAN", "060114154", "linda@hotmail.com", "TLL", "SMG");
    }

    public void initReferensi() {
        //INIT GRUP
        createReferensi(0L, "GP", "Grup Grup", "Grup of Grup, root Grup", 1, null, 1L, null, null, null);
        createReferensi(1L, "ICM", "Grup Incremental", "Grup Incremental", 1, null, 2L, null, null, null);
        createReferensi(1L, "PKT", "Grup Jenis Task", "Grup Jenis Task", 1, null, 3L, null, null, null);
        createReferensi(1L, "PKT", "Grup Pangkat", "Grup Pangkat", 1, null, 4L, null, null, null);
        createReferensi(1L, "JBT", "Grup Jabatan", "Grup Jabatan", 1, null, 5L, null, null, null);
        createReferensi(1L, "LMP", "Grup Lampiran", "Grup Lampiran", 1, null, 6L, null, null, null);
        createReferensi(1L, "POT", "Grup Posisi", "Grup Posisi Tim Audit", 1, null, 7L, null, null, null);
        createReferensi(1L, "PJK", "Grup Pajak", "Grup Jenis Pajak", 1, null, 8L, null, null, null);
        createReferensi(1L, "JPM", "Grup Jenis Pemeriksaan", "Grup Jenis Pemeriksaan", 1, null, 9L, null, null, null);
        //INIT CONTENT
        createReferensi(2L, "IPK", "Incremental Pemeriksaan", "Incremental Pemeriksaan", 1, null, 5L, 2012L, null, null);

        createReferensiUrut(3L, "DFN", "Daftar Nominasi", "Daftar Nominasi", 1, null, null, null, 0);
        createReferensiUrut(3L, "PKW", "Penugasan Kanwil", "Penugasan Kanwil", 1, null, null, null, 1);
        createReferensiUrut(3L, "LP2", "LP 2", "LP 2", 1, null, null, null, 2);
        createReferensiUrut(3L, "SP2", "SP2", "SP2", 1, null, null, null, 3);
        createReferensiUrut(3L, "LHP", "LHP", "LHP", 1, null, null, null, 4);
        createReferensiUrut(3L, "ADP", "Audit Plan", "Audit Plan", 1, null, null, null, 5);
        createReferensiUrut(3L, "SP3", "SP3", "SP3", 1, null, null, null, 6);


        createReferensiUrut(4L, "2C", "II C Pengatur", "Pelaksana", 1, null, null, null, 0);
        createReferensiUrut(4L, "2D", "II D Pengatur Tingkat I", "II D Pengatur Tingkat I", 1, null, null, null, 1);
        createReferensiUrut(4L, "3A", "III A Penata Muda", "III A Penata Muda", 1, null, null, null, 2);
        createReferensiUrut(4L, "3B", "III B Penata Muda Tingkat I", "III B Penata Muda Tingkat I", 1, null, null, null, 3);
        createReferensiUrut(4L, "3C", "III C Penata", "III C Penata", 1, null, null, null, 4);
        createReferensiUrut(4L, "3D", "III D Penata Tingkat I", "III D Penata Tingkat I", 1, null, null, null, 5);
        createReferensiUrut(4L, "4A", "IV A Pengatur", "Pelaksana", 1, null, null, null, 6);

        createReferensiUrut(5L, "SEK", "Kepala Seksi", "Kepala Seksi", 1, null, null, null, 0);
        createReferensiUrut(5L, "SVR", "Supervisor", "Supervisor", 1, null, null, null, 1);
        createReferensiUrut(5L, "ACR", "AR", "AR", 1, null, null, null, 2);
        createReferensiUrut(5L, "FGS", "Fungsional", "Fungsional", 1, null, null, null, 3);
        createReferensiUrut(5L, "PLK", "Pelaksana", "Pelaksana", 1, null, null, null, 4);


        createReferensi(6L, "LST", "Lampiran Surat Tugas", "Surat Tugas", 1, null, null, null, null, null);
        createReferensi(6L, "LKK", "Lampiran Kertas Kerja", "Kertas Kerja", 1, null, null, null, null, null);
        createReferensi(6L, "LLA", "Lampiran Laporan Hasil Audit", "Laporan Hasil Audit", 1, null, null, null, null, null);

        createReferensiUrut(7L, "SUV", "Supervisor", "Supervisor", 1, null, null, null, 0);
        createReferensiUrut(7L, "KTM", "Ketua Tim", "Ketua Tim", 1, null, null, null, 1);
        createReferensiUrut(7L, "AGT", "Anggota", "Anggota", 1, null, null, null, 2);

        createReferensiUrut(8L, "P21", "Pph 21", "Pph 21", 1, null, null, null, 0);
        createReferensiUrut(8L, "P24", "Pasal 24", "Pasal 24", 1, null, null, null, 1);
        createReferensiUrut(8L, "26A", "Pasal 26 A", "Pasal 26 A", 1, null, null, null, 2);

        createReferensiUrut(9L, "PLB", "Lebih Bayar", "Lebih Bayar", 1, null, null, null, 0);
        createReferensiUrut(9L, "KOM", "Kompensasi", "Kompensasi", 1, null, null, null, 1);
        createReferensiUrut(9L, "RTL", "Rugi Tidak Lebih Bayar", "Rugi Tidak Lebih Bayar", 1, null, null, null, 2);
        createReferensiUrut(9L, "PKS", "Khusus", "Khusus", 1, null, null, null, 3);
        createReferensiUrut(9L, "ITK", "Instruksi Kanwil", "Instruksi Kanwil", 1, null, null, null, 4);
        createReferensiUrut(9L, "ITP", "Instruksi Pusat", "Instruksi Pusat", 1, null, null, null, 5);
        createReferensiUrut(9L, "PHP", "Penghapusan", "Penghapusan", 1, null, null, null, 6);
        createReferensiUrut(9L, "PKO", "PPN Kompensasi", "PPN Kompensasi", 1, null, null, null, 7);
        createReferensiUrut(9L, "RMP", "Rutin Masa PPN", "Rutin Masa PPN", 1, null, null, null, 8);

        createReferensi(17L, "PMK", "Pemeriksaan", "Workflow Pemeriksaan", 1, null, null, null, null, null);
        createReferensi(18L, "STJ", "Setuju", "Keputusan Setuju", 1, null, null, null, null, null);
        createReferensi(18L, "TSJ", "Tidak Setuju", "Keputusan Tidak Setuju", 1, null, null, null, null, null);
        createReferensi(18L, "RVS", "Revisi", "Keputusan Revisi", 1, null, null, null, null, null);
        createReferensi(18L, "TLK", "Ditolak", "Penolakan", 1, null, null, null, null, null);
        createReferensi(18L, "OK", "OK", "OK", 1, null, null, null, null, null);


    }

    public void createGrup(String nama, String keterangan, String kode) {
        UserGrup ug = new UserGrup();
        ug.setNama(nama);
        ug.setKeterangan(keterangan);
        ug.setKode(kode);
        userGrupRepository.save(ug);
    }

    public void initGrup() {
        createGrup("Super User", "Super User all role no crud", "SU");
        createGrup("Auditor", "Auditor User Grup", "SA");
        createGrup("Director", "View All Summary", "SD");

    }

    public void createUserRole(String kode, String nama, String keterangan) {
        UserRole ur = new UserRole();
        ur.setKode(kode);
        ur.setKeterangan(keterangan);
        ur.setNama(nama);
        userRoleRepository.save(ur);
    }

    public void initUserRole() {
        createUserRole("NDP", "New Pemeriksaan", "New Pemeriksaan");
        createUserRole("EDR", "Edit Pemeriksaan", "Edit Pemeriksaan");

    }

    public void createWorkflowStatus(String kode, String nama, String kodeWorkflow) {
        Referensi rJenisWorkflow = referensiRepository.byGrupAndKode(17L, kodeWorkflow);
        WorkflowStatus ur = new WorkflowStatus();
        ur.setKode(kode);
        ur.setNama(nama);
        ur.setJenis(rJenisWorkflow);
        workflowStatusRepository.save(ur);
    }

    public void createUser(String username, String password, String kodeGrup, String email) {
        List<UserGrup> gs = userGrupRepository.lookupNamaKode(kodeGrup);
        UserUser u = new UserUser();
        u.setUsername(username);
        u.setPassword("go" + password);
        u.setEmail(email);
        u.setGrup(gs.size() > 0 ? gs.get(0) : null);
        userUserRepository.save(u);
    }

    public void initUser() {
        createUser("tejo.ak", "p4ssword", "SU", "tejo.ak@gmail.com");
        createUser("puput.ag", "p4ssword", "SU", "puput.anggraeni85@gmail.com");
    }

    public void initWorkflowStatus() {
        createWorkflowStatus("DFT", "Draft", "PMK");
    }

    public void reInitAll() {
        delAll();
        initReferensi();
        initUnit();
        initGrup();
        initUserRole();
        initWorkflowStatus();
        initPegawai();
        initUser();

    }


    public Referensi createReferensi(Long grup, String kode, String nama, String keterangan, Integer aktif, String isi, Long num, Long nilai, Referensi parentGrup, Referensi parent) {
        Referensi r = new Referensi();
        r.setGrup(grup);
        r.setKode(kode);
        r.setNama(nama);
        r.setKeterangan(keterangan);
        r.setAktif(aktif);
        r.setIsi(isi);
        r.setNum(num);
        r.setNilai(nilai);
        r.setParentGrup(parentGrup);
        r.setParent(parent);
        referensiRepository.save(r);
        return r;
    }

    public Referensi createReferensiUrut(Long grup, String kode, String nama, String keterangan, Integer aktif, String isi, Long num, Long nilai, Integer urut) {
        Referensi r = new Referensi();
        r.setGrup(grup);
        r.setKode(kode);
        r.setNama(nama);
        r.setKeterangan(keterangan);
        r.setAktif(aktif);
        r.setIsi(isi);
        r.setNum(num);
        r.setNilai(nilai);
        r.setUrut(urut);
        referensiRepository.save(r);
        return r;
    }
}
