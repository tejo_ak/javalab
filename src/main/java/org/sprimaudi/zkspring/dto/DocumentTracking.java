package org.sprimaudi.zkspring.dto;

import org.sprimaudi.zkspring.entity.Workflow;
import org.sprimaudi.zkutil.workflow.WorkflowableDokumen;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 2/13/13
 * Time: 9:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class DocumentTracking implements WorkflowableDokumen {
    private String id;
    private Date tanggal;
    private String nomor;
    private String kodeTipeDokumen;
    private String nomorDokumen;
    private Workflow workflow;
    private String deskripsi;
    private String status;

    public DocumentTracking() {
    }

    public DocumentTracking(String deskripsi, WorkflowableDokumen doc) {
        this.deskripsi = deskripsi;
        if (doc != null) {
            this.id = doc.getId();
            this.workflow = doc.getWorkflow();
            this.nomor = doc.getNomor();
            this.nomorDokumen = doc.getNomorDokumen();
            this.tanggal = doc.getTanggal();
            this.kodeTipeDokumen = doc.getKodeTipeDokumen();
            this.status = doc != null
                    && doc.getWorkflow() != null
                    && doc.getWorkflow().getStatus() != null
                    ? doc.getWorkflow().getStatus().getNama() : "";
        }
    }

    public DocumentTracking(WorkflowableDokumen doc, String deskripsi, String status) {
        this.deskripsi = deskripsi;
        this.status = status;
        if (doc != null) {
            this.workflow = doc.getWorkflow();
            this.id = doc.getId();
            this.nomor = doc.getNomor();
            this.nomorDokumen = doc.getNomorDokumen();
            this.tanggal = doc.getTanggal();
            this.kodeTipeDokumen = doc.getKodeTipeDokumen();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomorDokumen() {
        return nomorDokumen;
    }

    public void setNomorDokumen(String nomorDokumen) {
        this.nomorDokumen = nomorDokumen;
    }

    public Workflow getWorkflow() {
        return workflow;
    }

    public void setWorkflow(Workflow workflow) {
        this.workflow = workflow;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getKodeTipeDokumen() {
        return kodeTipeDokumen;
    }

    public void setKodeTipeDokumen(String kodeTipeDokumen) {
        this.kodeTipeDokumen = kodeTipeDokumen;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
