package org.sprimaudi.zkspring.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: lenovo
 * Date: 1/27/13
 * Time: 9:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class Validation {
    private boolean valid;
    private List<String> validations = new ArrayList<String>();

    public Validation() {
        valid = true;
    }

    public Validation(boolean valid) {
        this.valid = valid;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void add(String validation) {
        validations.add(validation);
        //Automatically set valid as false as any validation entry was set
        valid = false;
    }

    public List<String> getValidations() {
        return validations;
    }

    public String serializedValidation(String separator) {
        String vals = "";
        boolean isFirst = true;
        for (Iterator<String> iterator = validations.iterator(); iterator.hasNext(); ) {
            String val = iterator.next();
            String sepa = isFirst ? "" : separator;
            vals += (sepa + val);
            isFirst = false;
        }
        return vals;
    }
}
