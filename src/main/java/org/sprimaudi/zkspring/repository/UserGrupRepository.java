package org.sprimaudi.zkspring.repository;

import org.jote.util.dynaque.DynaqueExecutor;
import org.jote.util.dynaque.QueryParameterLogic;
import org.jote.util.dynaque.annotation.Dynaque;
import org.jote.util.dynaque.annotation.Dynaquery;
import org.jote.util.dynaque.annotation.QueryParameter;
import org.sprimaudi.zkspring.entity.UserGrup;
import org.sprimaudi.zkspring.entity.UserUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 8/11/12
 * Time: 8:00 AM
 * To change this template use File | Settings | File Templates.
 */
@Dynaque(dynaquery = {
        @Dynaquery(name = "user_grup_filter",
                base = "select ug from UserGrup ug where ug.created is not null",
                parameters = {
                        @QueryParameter(name = "searchable"),
                        @QueryParameter(name = "nama", field = "ug.nama", logic = QueryParameterLogic.OR, parent = "searchable"),
                        @QueryParameter(name = "keterangan", field = "ug.keterangan", logic = QueryParameterLogic.OR, parent = "searchable"),
                        @QueryParameter(name = "kode", field = "ug.kode", logic = QueryParameterLogic.OR, parent = "searchable")
                })
})
public interface UserGrupRepository extends CrudRepository<UserGrup, String>,
        DynaqueExecutor<UserGrup, String> {
    public static final String USER_GRUP_FILTER = "user_grup_filter";

    @Query("select g from UserGrup g where g.nama like ?1 or g.kode like ?1")
    List<UserGrup> lookupNamaKode(String lookupParams);
}
