package org.sprimaudi.zkspring.repository;

import org.jote.util.dynaque.DynaqueExecutor;
import org.jote.util.dynaque.NullMeans;
import org.jote.util.dynaque.QueryParameterLogic;
import org.jote.util.dynaque.annotation.Dynaque;
import org.jote.util.dynaque.annotation.Dynaquery;
import org.jote.util.dynaque.annotation.QueryParameter;
import org.sprimaudi.zkspring.entity.Referensi;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 7/21/12
 * Time: 5:19 PM
 * To change this template use File | Settings | File Templates.
 */
@Dynaque(dynaquery = {
        @Dynaquery(name = "referensi_filter",
                base = "select r from Referensi r where r.created!=null",
                parameters = {
                        @QueryParameter(name = "grup", nullMeans = NullMeans.IGNORE, logic = QueryParameterLogic.AND, field = "r.grup"),
                        @QueryParameter(name = "searchable", nullMeans = NullMeans.IGNORE, logic = QueryParameterLogic.AND),
                        @QueryParameter(name = "kode", nullMeans = NullMeans.IGNORE, logic = QueryParameterLogic.OR, field = "r.kode", parent = "searchable"),
                        @QueryParameter(name = "nama", nullMeans = NullMeans.IGNORE, logic = QueryParameterLogic.OR, field = "r.nama", parent = "searchable"),
                        @QueryParameter(name = "keterangan", nullMeans = NullMeans.IGNORE, logic = QueryParameterLogic.OR, field = "r.keterangan", parent = "searchable")


                })

})
public interface ReferensiRepository extends
        CrudRepository<Referensi, String>,
        DynaqueExecutor<Referensi, String> {
    public static final String REFERENSI_FILTER = "referensi_filter";

    @Query("from Referensi r where r.grup=? and kode=?")
    public Referensi byGrupAndKode(Long grup, String kode);

    @Query("from Referensi r where r.grup=?1 and urut=?2")
    public List<Referensi> byGrupAndUrut(Long grup, int urut);

    @Query("from Referensi r where r.grup=? order by urut,num")
    public List<Referensi> byGrup(Long grup);

    @Query("from Referensi r where r.grup=1 and r.num=?1")
    public Referensi getParentGrup(Long grup);

    @Query("select r from Referensi r,Referensi pg where r.parent=pg and r.grup=?1 and pg.id=?2")
    public List<Referensi> findGrupDanParent(Long grup, String parentGrup);

    @Query("from Referensi r where r.grup=?2 and r.kode=?1")
    Referensi byKodeGrup(String kode, Long grup);
}
