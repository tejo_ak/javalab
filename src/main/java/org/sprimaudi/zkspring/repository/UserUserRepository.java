package org.sprimaudi.zkspring.repository;

import org.jote.util.dynaque.DynaqueExecutor;
import org.jote.util.dynaque.QueryParameterLogic;
import org.jote.util.dynaque.annotation.Dynaque;
import org.jote.util.dynaque.annotation.Dynaquery;
import org.jote.util.dynaque.annotation.QueryParameter;
import org.sprimaudi.zkspring.entity.UserUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 8/11/12
 * Time: 8:00 AM
 * To change this template use File | Settings | File Templates.
 */
@Dynaque(dynaquery =
        {
                @Dynaquery(name = "user_filter", base = "select u from UserUser u where u.created is not null",
                        parameters = {
                                @QueryParameter(name = "search"),
                                @QueryParameter(name = "username", parent = "search", logic = QueryParameterLogic.OR, field = "u.username"),
                                @QueryParameter(name = "grup", parent = "search", logic = QueryParameterLogic.OR, field = "u.grup.nama"),
                                @QueryParameter(name = "email", parent = "search", logic = QueryParameterLogic.OR, field = "u.email"),
                                @QueryParameter(name = "nama", parent = "search", logic = QueryParameterLogic.OR, field = "u.pegawai.nama")
                        })
        })
public interface UserUserRepository extends
        CrudRepository<UserUser, String>,
        DynaqueExecutor<UserUser, String> {
    public static final String USER_FILTER = "user_filter";

    @Query("select u from UserUser u where u.username=?1")
    public List<UserUser> getLongin(String username);

    @Query("select u from UserUser u where u.username=?1 and (password =?2 or password=?3)")
    public List<UserUser> cekLogin(String username, String password, String goPassword);

    @Query("select u from UserUser u where u.email=?1")
    public List<UserUser> getEmailOfUser(String email);

    @Query("select u from UserUser u where u.aktivasi=?1")
    public List<UserUser> getAktivasi(String aktivasi);

    @Query("select u from UserUser u where u.pegawai.id=?1")
    public List<UserUser> getOfPegawai(String idPegawai);
}
