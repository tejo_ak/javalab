package org.sprimaudi.zkspring.repository;

import org.sprimaudi.zkspring.entity.Workflow;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 8/9/12
 * Time: 11:59 PM
 * To change this template use File | Settings | File Templates.
 */
public interface WorkflowRepository extends CrudRepository<Workflow, String> {
    @Query("select w from Workflow w,Workflow c where w.current = c and c.id=?1 order by w.created")
    public List<Workflow> findByCurrent(String idCurrent);

    @Transactional(readOnly = false)
    @Modifying
    @Query("update Workflow w set w.current=?2 where w.current =?1")
    public void updateCurrent(Workflow old, Workflow latest);

    @Transactional(readOnly = false)
    @Modifying
    @Query("delete Workflow w where w.current =?1")
    public void deleteCurrent(Workflow latest);
}
