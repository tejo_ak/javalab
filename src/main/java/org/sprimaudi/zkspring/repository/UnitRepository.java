package org.sprimaudi.zkspring.repository;

import org.jote.util.dynaque.DynaqueExecutor;
import org.jote.util.dynaque.QueryParameterLogic;
import org.jote.util.dynaque.annotation.Dynaque;
import org.jote.util.dynaque.annotation.Dynaquery;
import org.jote.util.dynaque.annotation.QueryParameter;
import org.sprimaudi.zkspring.entity.Unit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/23/12
 * Time: 8:24 PM
 * To change this template use File | Settings | File Templates.
 */
@Dynaque(dynaquery =
        {
                @Dynaquery(name = "unit_filter", base = "select u from Unit u where u.nama is not null",
                        parameters = {
                                @QueryParameter(name = "search"),
                                @QueryParameter(name = "nama", parent = "search", logic = QueryParameterLogic.OR, field = "u.nama"),
                                @QueryParameter(name = "kode", parent = "search", logic = QueryParameterLogic.OR, field = "u.kode"),
                                @QueryParameter(name = "alamat", parent = "search", logic = QueryParameterLogic.OR, field = "u.alamat")
                        })
        })
public interface UnitRepository extends
        CrudRepository<Unit, String>,
        DynaqueExecutor<Unit, String> {
    public static final String UNIT_FILTER = "unit_filter";

    @Query("select u from Unit u where u.kode=?1")
    Unit getByKode(String kode);
}
