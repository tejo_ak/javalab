package org.sprimaudi.zkspring.repository;

import org.jote.util.dynaque.DynaqueExecutor;
import org.jote.util.dynaque.QueryParameterLogic;
import org.jote.util.dynaque.annotation.Dynaque;
import org.jote.util.dynaque.annotation.Dynaquery;
import org.jote.util.dynaque.annotation.QueryParameter;
import org.sprimaudi.zkspring.entity.UserGrup;
import org.sprimaudi.zkspring.entity.UserGrupRole;
import org.sprimaudi.zkspring.entity.UserRole;
import org.sprimaudi.zkspring.entity.UserUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 8/11/12
 * Time: 8:00 AM
 * To change this template use File | Settings | File Templates.
 */
@Dynaque(dynaquery = {
        @Dynaquery(name = "user_role_filter",
                base = "select ug from UserRole ug where ug.created is not null",
                parameters = {
                        @QueryParameter(name = "searchable"),
                        @QueryParameter(name = "nama", field = "ug.nama", logic = QueryParameterLogic.OR, parent = "searchable"),
                        @QueryParameter(name = "keterangan", field = "ug.keterangan", logic = QueryParameterLogic.OR, parent = "searchable"),
                        @QueryParameter(name = "kode", field = "ug.kode", logic = QueryParameterLogic.OR, parent = "searchable")
                })
})
public interface UserRoleRepository extends CrudRepository<UserRole, String>,
        DynaqueExecutor<UserGrup, String> {
    public static final String USER_ROLE_FILTER = "user_role_filter";

    @Query("select rl from UserRole rl where rl not in ?1")
    public List<UserRole> findExceptAssigned(List<UserRole> roles);

    @Query("select rl from UserRole rl,UserGrupRole gl where rl=gl.role and gl in ?1")
    public List<UserRole> findAssigned(List<UserGrupRole> groles);
}
