package org.sprimaudi.zkspring.repository;

import org.sprimaudi.zkspring.entity.WorkflowStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 8/11/12
 * Time: 9:03 AM
 * To change this template use File | Settings | File Templates.
 */
public interface WorkflowStatusRepository extends CrudRepository<WorkflowStatus, String> {
    public WorkflowStatus findByKode(String kode);

    @Query("select ws from WorkflowStatus ws, Referensi r where ws.jenis=r and r.kode=?1 and ws.kode=?2 and r.grup=17")
    public WorkflowStatus findByJenisKode(String kodeJenis, String kode);

    @Query("select ws from WorkflowStatus ws where ws.jenis.id=?1")
    public List<WorkflowStatus> findByJenisWorkflow(String workflowId);

    @Query("select ws from WorkflowStatus ws, Referensi j where ws.jenis=j and j.kode=?1 and j.grup=?2")
    public List<WorkflowStatus> findByKodeReferensiJenis(String kode, Long grup);

    @Query("select ws from WorkflowStatus ws where ws.jenis.id=?1 and ws.kode in ?2")
    public List<WorkflowStatus> findByJenisKodesWorkflow(String workflowId, List<String> kodes);


}
