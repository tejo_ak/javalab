package org.sprimaudi.zkspring.repository;

import org.jote.util.dynaque.DynaqueExecutor;
import org.jote.util.dynaque.annotation.Dynaque;
import org.jote.util.dynaque.annotation.Dynaquery;
import org.jote.util.dynaque.annotation.QueryParameter;
import org.sprimaudi.zkspring.entity.UserActivity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created with IntelliJ IDEA.
 * User: lenovo
 * Date: 1/28/13
 * Time: 3:22 PM
 * To change this template use File | Settings | File Templates.
 */
@Dynaque(dynaquery = {
        @Dynaquery(name = "user_activity_filter",
                base = "select ua from UserActivity ua,UserUser u where ua.user=u",
                parameters = {
                        @QueryParameter(name = "user", field = "ua.user.username"),
                        @QueryParameter(name = "grup", field = "ua.grup")
                })
})
public interface UserActivityRepository
        extends CrudRepository<UserActivity, String>,
        DynaqueExecutor<UserActivity, String> {
}
