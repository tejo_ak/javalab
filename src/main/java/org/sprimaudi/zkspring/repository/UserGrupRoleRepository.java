package org.sprimaudi.zkspring.repository;

import org.sprimaudi.zkspring.entity.UserGrup;
import org.sprimaudi.zkspring.entity.UserGrupRole;
import org.sprimaudi.zkspring.entity.UserRole;
import org.sprimaudi.zkspring.entity.UserUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 8/11/12
 * Time: 8:00 AM
 * To change this template use File | Settings | File Templates.
 */
public interface UserGrupRoleRepository extends CrudRepository<UserGrupRole, String> {
    @Query("select ugr from UserGrupRole ugr where ugr.grup=?1")
    public List<UserGrupRole> findRoles(UserGrup grup);

    @Query("select ugr from UserGrupRole ugr where ugr.grup=?1 and ugr.role=?2")
    public List<UserGrupRole> findRoles(UserGrup grup, UserRole role);
}
