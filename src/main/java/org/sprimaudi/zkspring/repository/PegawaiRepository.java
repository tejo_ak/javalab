package org.sprimaudi.zkspring.repository;

import org.jote.util.dynaque.DynaqueExecutor;
import org.jote.util.dynaque.QueryParameterLogic;
import org.jote.util.dynaque.annotation.Dynaque;
import org.jote.util.dynaque.annotation.Dynaquery;
import org.jote.util.dynaque.annotation.QueryParameter;
import org.sprimaudi.zkspring.entity.Pegawai;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 7/28/12
 * Time: 10:46 AM
 * To change this template use File | Settings | File Templates.
 */
@Dynaque(dynaquery =
        {
                @Dynaquery(name = "pegawai_filter", base = "select p from Pegawai p where p.created is not null",
                        parameters = {
                                @QueryParameter(name = "search"),
                                @QueryParameter(name = "nama", parent = "search", logic = QueryParameterLogic.OR, field = "p.nama"),
                                @QueryParameter(name = "nip", parent = "search", logic = QueryParameterLogic.OR, field = "p.nip"),
                                @QueryParameter(name = "email", parent = "search", logic = QueryParameterLogic.OR, field = "p.email"),
                                @QueryParameter(name = "jabatan", parent = "search", logic = QueryParameterLogic.OR, field = "p.jabatan.nama"),
                                @QueryParameter(name = "unit", parent = "search", logic = QueryParameterLogic.OR, field = "p.unit.nama")
                        })
        })
public interface PegawaiRepository extends
        CrudRepository<Pegawai, String>,
        DynaqueExecutor<Pegawai, String> {
    public static final String PEGAWAI_FILTER = "pegawai_filter";

    @Query("select p from Pegawai p where p.nip like ?1 or p.nama like ?1")
    public List<Pegawai> lookupNipNama(String lookup);

    @Query("select p from Pegawai p where p.email= ?1  ")
    public List<Pegawai> getEmailOfPegawai(String email);
}
