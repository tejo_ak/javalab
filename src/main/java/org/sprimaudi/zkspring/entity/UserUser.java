package org.sprimaudi.zkspring.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 8/9/12
 * Time: 11:29 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class UserUser extends Entitas {

    @Column(length = 64)
    private String username;
    @Column(length = 64)
    private String email;
    @Column(length = 6)
    private String aktivasi;
    @Column(length = 72)
    private String password;
    @OneToOne(targetEntity = Pegawai.class)
    private Pegawai pegawai;


    @ManyToOne(targetEntity = UserGrup.class)
    private UserGrup grup;

    @OneToMany(targetEntity = Workflow.class, mappedBy = "user")
    private List<Workflow> workflowByUser;

    @OneToMany(targetEntity = UserActivity.class, mappedBy = "user")
    private List<UserActivity> activityOfUser;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Pegawai getPegawai() {
        return pegawai;
    }

    public void setPegawai(Pegawai pegawai) {
        this.pegawai = pegawai;
    }

    public UserGrup getGrup() {
        return grup;
    }

    public void setGrup(UserGrup grup) {
        this.grup = grup;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAktivasi() {
        return aktivasi;
    }

    public void setAktivasi(String aktivasi) {
        this.aktivasi = aktivasi;
    }
}
