package org.sprimaudi.zkspring.entity;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: lenovo
 * Date: 1/28/13
 * Time: 3:19 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class UserActivity extends Entitas {


    @ManyToOne(targetEntity = UserUser.class)
    private UserUser user;
    private String description;
    @Column(length = 32)
    private String ip;
    @Column(length = 16)
    private String grup;


    public UserUser getUser() {
        return user;
    }

    public void setUser(UserUser user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getGrup() {
        return grup;
    }

    public void setGrup(String grup) {
        this.grup = grup;
    }
}
