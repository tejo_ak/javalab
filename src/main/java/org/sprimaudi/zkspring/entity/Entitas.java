package org.sprimaudi.zkspring.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: lenovo
 * Date: 2/3/13
 * Time: 8:48 PM
 * To change this template use File | Settings | File Templates.
 */
@MappedSuperclass
public class Entitas implements Serializable {
    @Id
    @Column(length = 32)
    protected String id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(length = 19)
    protected Date created;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(length = 19)
    protected Date updated;


    @Column(length = 64)
    protected String createdBy;
    @Column(length = 64)
    protected String updatedBy;

    @PrePersist
    public void onPrepersist() {
        if (this.id != null && !"".equals(this.id)) {
//            System.out.println("id is present, doesnt generate any id");
        } else {
            this.id = UUID.randomUUID().toString().replace("-", "");
        }
        if (created == null) {
            created = new Date();
        }
        if (updated == null) {
            updated = new Date();
        }

    }

    @PreUpdate
    public void onPreupdate() {
        updated = new Date();
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
