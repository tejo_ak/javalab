package org.sprimaudi.zkspring.entity;


import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 8/9/12
 * Time: 11:35 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class UserGrupRole extends  Entitas {

    @ManyToOne(targetEntity = UserRole.class)
    private UserRole role;
    @ManyToOne(targetEntity = UserGrup.class)
    private UserGrup grup;



    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public UserGrup getGrup() {
        return grup;
    }

    public void setGrup(UserGrup grup) {
        this.grup = grup;
    }
}
