/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sprimaudi.zkspring.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author UserUser
 */
@Entity
public class Referensi extends Entitas implements Serializable {


    private Long grup;
    @Column(length = 6)
    private String kode;
    @Column(length = 64)
    private String nama;
    @Column(length = 512)
    private String keterangan;
    private Integer aktif = 1;
    @Column(length = 64)
    private String isi;
    private Long num;
    private Integer urut;
    private Long nilai;
    @OneToOne(targetEntity = Referensi.class)
    private Referensi parentGrup;
    @ManyToOne(targetEntity = Referensi.class)
    private Referensi parent;

    @OneToMany(targetEntity = Referensi.class, mappedBy = "parent")
    private List<Referensi> childs;



    public Referensi getParent() {
        return parent;
    }

    public void setParent(Referensi parent) {
        this.parent = parent;
    }

    public Referensi getParentGrup() {
        return parentGrup;
    }

    public void setParentGrup(Referensi parentGrup) {
        this.parentGrup = parentGrup;
    }

    //=====================
    //list of reference type
    //=====================


    @OneToMany(mappedBy = "jenis", targetEntity = WorkflowStatus.class)
    List<WorkflowStatus> workflowStatusByJenis;
    @OneToMany(mappedBy = "jenis", targetEntity = Workflow.class)
    List<Workflow> workflowByJenis;
    @OneToMany(mappedBy = "keputusan", targetEntity = Workflow.class)
    List<Workflow> workflowByKeputusan;


    public Long getGrup() {
        return grup;
    }

    public void setGrup(Long grup) {
        this.grup = grup;
    }


    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public Long getNilai() {
        return nilai;
    }

    public void setNilai(Long nilai) {
        this.nilai = nilai;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public Integer getAktif() {
        return aktif;
    }

    public void setAktif(Integer aktif) {
        this.aktif = aktif;
    }

    public Integer getUrut() {
        return urut;
    }

    public void setUrut(Integer urut) {
        this.urut = urut;
    }
}
