package org.sprimaudi.zkspring.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/20/12
 * Time: 7:46 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Pegawai extends Entitas implements Serializable {

    @Column(length = 64)
    private String nama;
    @Column(length = 64)
    private String email;
    @Column(length = 18)
    private String nip;
    @ManyToOne(targetEntity = Referensi.class)
    private Referensi pangkat;
    @ManyToOne(targetEntity = Referensi.class)
    private Referensi jabatan;
    @ManyToOne(targetEntity = Unit.class)
    private Unit unit;


    //    @OneToMany(mappedBy = "objectAudit", targetEntity = ObjectAudit.class)
//    List<Budget> auditees;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public Referensi getPangkat() {
        return pangkat;
    }

    public void setPangkat(Referensi pangkat) {
        this.pangkat = pangkat;
    }

    public Referensi getJabatan() {
        return jabatan;
    }

    public void setJabatan(Referensi jabatan) {
        this.jabatan = jabatan;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

