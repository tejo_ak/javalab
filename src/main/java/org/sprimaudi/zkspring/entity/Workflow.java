package org.sprimaudi.zkspring.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 8/3/12
 * Time: 12:38 AM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Workflow extends Entitas implements Serializable {

    @Column(length = 32)
    private String docId;
    private String nomor;
    @Temporal(TemporalType.DATE)
    private Date tanggal;

    private Integer controller;
    @ManyToOne
    private Referensi keputusan;

    //    Relationship
    @ManyToOne(targetEntity = UserUser.class)
    private UserUser user;

    @ManyToOne(targetEntity = Referensi.class)
    Referensi jenis;
    @Column(length = 512)
    private String catatan;

    @ManyToOne(targetEntity = Workflow.class)
    private Workflow current;

    @OneToMany(targetEntity = Workflow.class, mappedBy = "current")
    private List<Workflow> histories;

    @ManyToOne(targetEntity = WorkflowStatus.class)
    private WorkflowStatus status;


    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }


    public Integer getController() {
        return controller;
    }

    public void setController(Integer controller) {
        this.controller = controller;
    }

    public UserUser getUser() {
        return user;
    }

    public void setUser(UserUser user) {
        this.user = user;
    }

    public Referensi getJenis() {
        return jenis;
    }

    public void setJenis(Referensi jenis) {
        this.jenis = jenis;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public WorkflowStatus getStatus() {
        return status;
    }

    public void setStatus(WorkflowStatus status) {
        this.status = status;
    }

    public Workflow getCurrent() {
        return current;
    }

    public void setCurrent(Workflow current) {
        this.current = current;
    }

    public List<Workflow> getHistories() {
        return histories;
    }

    public void setHistories(List<Workflow> histories) {
        this.histories = histories;
    }

    public Referensi getKeputusan() {
        return keputusan;
    }

    public void setKeputusan(Referensi keputusan) {
        this.keputusan = keputusan;
    }
}
