package org.sprimaudi.zkspring.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 8/10/12
 * Time: 7:41 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class WorkflowStatus extends Entitas {

    @Column(length = 64)
    private String nama;
    @Column(length = 256)
    private String keterangan;
    @ManyToOne(targetEntity = Referensi.class)
    private Referensi jenis;
    @Column(length = 6)
    private String kode;
    @OneToMany(targetEntity = Workflow.class, mappedBy = "status")
    private List<Workflow> status;


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Referensi getJenis() {
        return jenis;
    }

    public void setJenis(Referensi jenis) {
        this.jenis = jenis;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public List<Workflow> getStatus() {
        return status;
    }

    public void setStatus(List<Workflow> status) {
        this.status = status;
    }
}
