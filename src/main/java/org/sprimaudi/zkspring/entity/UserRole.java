package org.sprimaudi.zkspring.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 8/9/12
 * Time: 11:31 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class UserRole extends Entitas {

    @Column(length = 24)
    private String nama;
    @Column(length = 32)
    private String kode;
    @Column(length = 128)
    private String keterangan;

    @OneToMany(targetEntity = UserGrupRole.class, mappedBy = "role")
    private List<UserGrupRole> roles;


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public List<UserGrupRole> getRoles() {
        return roles;
    }

    public void setRoles(List<UserGrupRole> roles) {
        this.roles = roles;
    }
}
