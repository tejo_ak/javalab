package org.sprimaudi.zkspring.util;

import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: lenovo
 * Date: 1/20/13
 * Time: 6:33 AM
 * To change this template use File | Settings | File Templates.
 */
public class LogConf {

    public LogConf(Properties props) {
        Iterator i = props.keySet().iterator();
        while (i.hasNext()) {
            String loggerName = (String) i.next();
            String levelName = props.getProperty(loggerName);
            try {
                Level level = Level.parse(levelName);
                Logger l = Logger.getLogger(loggerName);
                l.setLevel(level);
            } catch (IllegalArgumentException e) {
                System.err.println("WARNING: Unable to parse '"
                        + levelName + "' as a java.util.Level for logger "
                        + loggerName + "; ignoring...");
            }
        }
    }
}
