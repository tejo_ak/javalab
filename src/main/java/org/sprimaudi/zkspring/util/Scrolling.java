package org.sprimaudi.zkspring.util;

import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: lenovo
 * Date: 2/5/13
 * Time: 4:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class Scrolling<T> {
    Scroller<T> scroller;
    int size = 20;

    public Scrolling(int pageSize, Scroller<T> scroller) {
        this.scroller = scroller;
        this.size = pageSize;
    }

    public void scrolls() {
        if (scroller == null) {
            return;
        }
        boolean lanjut = true;
        int page = 0;
        while (lanjut) {
            int offset = page * size;
            List<T> entities = scroller.fetch(offset, size);
            for (Iterator<T> iterator = entities.iterator(); iterator.hasNext(); ) {
                T entity = iterator.next();
                scroller.scroll(entity);
            }
            lanjut = (entities.size() > 0);
            page++;
        }
    }
}
