package org.sprimaudi.zkspring.util;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:25 AM
 * To change this template use File | Settings | File Templates.
 */
@Component(value = "pgm")
@Scope("page")
public class PageMgt {

    public final static String MODAL_RESULT_IDENTIFIER = "modal_result_identifier";
    public final static int MODAL_RESULT_CANCEL = 1;
    public final static int MODAL_RESULT_SUCCEDED = 2;


    public Include mainInclude;
    public Include subInclude;
    public Include navInclude;
    public Include propInclude;
    public Include menuInclude;

    public Center mainPanel;
    public South subPanel;
    public West navPanel;
    public East propPanel;

    public Window mainWindow;

    private List<Window> loadedWindows = new ArrayList<Window>();


    private static final String WINDOW_PARAM_ATTR = "window_param_attr";
    private static final String INCLUDE_PARAM_ATTR = "include_param_attr";

    public int getModalResult(Window win) {
        Integer oid = (Integer) win.getAttribute(MODAL_RESULT_IDENTIFIER);
        return (oid != null) ? oid : null;
    }

    public boolean checkModalResult(Window win, int modalResult) {
        int rslt = getModalResult(win);
        return modalResult == rslt;
    }

    public void setModalResult(Window win, int result) {
        win.setAttribute(MODAL_RESULT_IDENTIFIER, result);
    }

    public String tesBean() {

        return "dektop scope bean " + UUID.randomUUID().toString();
    }


    public void initInclude(Include main, Include sub, Include nav, Include prop, Include menu) {
        this.mainInclude = main;
        this.subInclude = sub;
        this.navInclude = nav;
        this.propInclude = prop;
        this.menuInclude = menu;
    }

    public void initPanel(Center mainPanel, South subPanel, West navPanel, East propPanel) {
        this.mainPanel = mainPanel;
        this.subPanel = subPanel;
        this.navPanel = navPanel;
        this.propPanel = propPanel;
    }


    public void initWindow(Window mainWindow) {
        this.mainWindow = mainWindow;
    }


    public void closePanels() {
        this.subPanel.setOpen(false);
        //Tunda menutup nav Panel, karena nav panel diperlakukan khusus sebagai panel untuk menu
        broadcast("onCloseNavigation", new HashMap<String, Object>());

        //this.navPanel.setOpen(false);
        this.propPanel.setOpen(false);
    }

    public void closeProp() {
        closePanel(this.propPanel);
    }

    public void closeNav() {
        closePanel(this.navPanel);
    }

    private void closePanel(LayoutRegion region) {
        region.setOpen(false);
    }

    public void showMain(String pagePath, Map<String, Object> arg) {
        showPage(mainInclude, pagePath, false, mainPanel, false, arg);
    }

    public void showMenu(String pagePath, Map<String, Object> arg) {
        showPage(menuInclude, pagePath, false, null, false, arg);
    }

    public void showMainOnly(String pagePath, Map<String, Object> arg) {

        showPage(mainInclude, pagePath, false, mainPanel, true, arg);
    }

    public void showMainOnly(String pagePath) {

        showPage(mainInclude, pagePath, false, mainPanel, true);
    }

    public void showMain(String pagePath) {
        showPage(mainInclude, pagePath, false, mainPanel, false);
    }


    public void showNav(String pagePath) {
        showNav(pagePath, new HashMap<String, Object>());
    }

    public void showNav(String pagePath, Map<String, Object> arg) {
        arg.put("zulpath", pagePath);
        broadcast("onNavigationShow", arg);
        //showPage(navInclude, pagePath, false, navPanel, false, arg);
    }

    public void showNavigation(String pagePath, Map<String, Object> arg) {
        showPage(navInclude, pagePath, false, navPanel, false, arg);
    }

    public void showProp(String pagePath) {
        showPage(propInclude, pagePath, false, propPanel, false);
    }

    public void showProp(String pagePath, Map<String, Object> arg) {
        showPage(propInclude, pagePath, false, propPanel, false, arg);
    }

    public Window showModal(String pagePath, Map<String, Object> arg, String width) {
        org.zkoss.zk.ui.Component c = Executions.createComponents(pagePath, null, arg);
        if (c instanceof Window) {
            Window w = (Window) c;
            w.setClosable(true);
            if (width != null && !"".equals(width)) {
                w.setWidth(width);
            }
            Events.echoEvent("onAfterCreate", c, arg);
            //add on cancel
            if (!w.getEventHandlerNames().contains("onCancel")) {

                w.addEventListener("onCancel", new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        event.getTarget().detach();
                    }
                });
                w.addEventListener("onAfterCreate", new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        Window w = (Window) event.getTarget();
                        w.focus();
                    }
                });
            }
            w.setPosition("center");
            w.setSizable(true);
            w.doModal();
            return w;
        }
        return null;
    }

    public Window showPopup(String pagePath, Map<String, Object> arg, String width) {
        org.zkoss.zk.ui.Component c = Executions.createComponents(pagePath, null, arg);
        if (c instanceof Window) {
            Window w = (Window) c;
            w.setClosable(true);
            if (width != null && !"".equals(width)) {
                w.setWidth(width);
            }
            Events.echoEvent("onAfterCreate", c, arg);
            w.setPosition("right,center");
            w.setSizable(true);
            w.doOverlapped();
            return w;
        }
        return null;
    }

    public Window showModal(String pagePath, Map<String, Object> arg) {
        return showModal(pagePath, arg, null);
    }

    public Window showPopup(String pagePath, Map<String, Object> arg) {
        return showPopup(pagePath, arg, null);
    }

    public void showInclude(Include icl, String pagePath, Map<String, Object> arg) {
        showPage(icl, pagePath, false, null, false, arg);
    }

    public void showPage(Include icl,
                         String pagePath,
                         boolean progressing,
                         LayoutRegion region,
                         boolean closeOther) {
        showPage(icl, pagePath, progressing, region, closeOther, null);
    }

    public void showPage(Include icl,
                         String pagePath,
                         boolean progressing,
                         LayoutRegion region,
                         boolean closeOther,
                         Map<String, Object> arg) {
        icl.setProgressing(progressing);
        if (closeOther) {
            closePanels();
        }
        icl.setDynamicProperty(INCLUDE_PARAM_ATTR, arg);
        Window oldWin = findChildWindow(icl);
        if (oldWin != null && !oldWin.isInvalidated()) {
            //this is the old window content of include;
            Events.postEvent("onBeforeDetache", oldWin, arg);
        }
        icl.setSrc(pagePath);
        Window win = findChildWindow(icl);

        if (region != null && win.getTitle() != null && !"".equals(region.getTitle()) && !(region instanceof Center)) {
            region.setTitle("Panel " + win.getTitle());
        }
        postOnAfterCreate(win, arg);
        if (region != null && !(region instanceof Center)) {
            region.setOpen(true);
        }
    }

    private void addWindow(Window window) {
        if (window == null) {
            return;
        }
        List<Window> detached = new ArrayList<Window>();
        boolean foundsimilar = false;
        for (Iterator<Window> iterator = loadedWindows.iterator(); iterator.hasNext(); ) {
            Window wnd = iterator.next();
//            System.out.println(String.format("%s -- %s", wnd.getId(), wnd.getTitle()));
            if (wnd.isInvalidated()) {
                detached.add(wnd);
            }
            if (window.equals(wnd)) {
                foundsimilar = true;
                break;
            }
        }
        if (!foundsimilar) {
            loadedWindows.add(window);
        }
        loadedWindows.removeAll(detached);
        detached.clear();
//        loadedWindows.add(window);
    }

    public boolean cekPopupWin(Window win) {
        return win != null && !win.isInvalidated();
    }

    private void cleanWindow() {
        List<Window> detached = new ArrayList<Window>();
        for (Iterator<Window> iterator = loadedWindows.iterator(); iterator.hasNext(); ) {
            Window wnd = iterator.next();
            if (wnd.isInvalidated()) {
                detached.add(wnd);
            }
        }
        loadedWindows.removeAll(detached);
        detached.clear();
    }


    public Window findChildWindow(Include include) {
        Collection<org.zkoss.zk.ui.Component> childs = include.getFellows();
        for (org.zkoss.zk.ui.Component child : childs) {
            if (child instanceof Window) {
                return (Window) child;
            }
        }
        return null;
    }

    private void postOnAfterCreate(Window win, Map<String, Object> arg) {
        if (win != null) {
            Events.echoEvent("onAfterCreate", win, arg);
            addWindow(win);
        }
    }

    public <T> T eventParam(Class<T> clazz, Event event, String key) {
        Object odata = event.getData();
        if (odata == null)
            return null;
        if (!(odata instanceof Map))
            return null;
        Object opar = ((Map<String, Object>) odata).get(key);
        if (opar == null)
            return null;
        return (T) opar;
    }

    public <T> T windowParam(Class<T> clazz, Window window, String key) {
        Object odata = window.getDesktop().getExecution().getAttribute(INCLUDE_PARAM_ATTR);
        if (odata == null)
            return null;
        if (!(odata instanceof Map))
            return null;
        Object opar = ((Map<String, Object>) odata).get(key);
        if (opar == null)
            return null;
        return (T) opar;
    }

    public void broadcast(String eventName, Map<String, Object> args) {
        List<Window> detached = new ArrayList<Window>();
        Events.echoEvent(eventName, this.mainWindow, args);
        for (Iterator<Window> iterator = loadedWindows.iterator(); iterator.hasNext(); ) {
            Window wnd = iterator.next();
            if (wnd.isInvalidated()) {
                detached.add(wnd);
            } else {
                Events.echoEvent(eventName, wnd, args);
            }
        }
        loadedWindows.removeAll(detached);
        detached.clear();
    }

    public Map<String, Object> forward(Event evt) {
        return (Map<String, Object>) evt.getData();
    }

    @PostConstruct
    public void init() {
    }

    public static int beanno = 0;

    @PreDestroy
    public void destroy() {
    }
}
