package org.sprimaudi.zkspring.util;

import com.djbc.utilities.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sprimaudi.zkspring.entity.UserGrupRole;
import org.sprimaudi.zkspring.entity.UserUser;
import org.sprimaudi.zkspring.repository.UserGrupRoleRepository;
import org.sprimaudi.zkspring.repository.UserUserRepository;
import org.sprimaudi.zkspring.service.UserService;
import org.sprimaudi.zkutil.workflow.WorkflowableDokumen;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import javax.inject.Inject;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:25 AM
 * To change this template use File | Settings | File Templates.
 */
@Component(value = "usr")
@Scope(value = "desktop", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserMgt {

    private UserUser user;
    private List<UserGrupRole> roles = new ArrayList<UserGrupRole>();
    private boolean logged;


    @Inject
    UserService userService;

    @Inject
    UserUserRepository userUserRepository;

    @Inject
    UserGrupRoleRepository userGrupRoleRepository;

    @Inject
    PageMgt pgm;

    @Inject
    Mapper mapper;

    Logger logger = LoggerFactory.getLogger(UserMgt.class);

    public void logout() {
        user = null;
        roles.clear();
        logged = false;
        logger.info(String.format("user [%s] is logging out ", user != null ? user.getUsername() : "no username information"));
        pgm.broadcast("onLoginLoggedOut", mapper.map("waktu", new Date()));
    }

    public void resolveLogin(String username) {
        roles.clear();

        user = userService.getLogin(username);
        if (user != null) {
            logger.info(String.format("user [%s] login is resolved", username));
            logged = true;
            roles.addAll(userGrupRoleRepository.findRoles(user.getGrup()));
            pgm.broadcast("onLoginResolved", mapper.map("user", user));
        } else {
            logger.debug(String.format("user [%s] is attemting to login but cannot resolve login", username));
        }

    }

    public boolean hasRole(String kode) {
//        if (user != null && user.getGrup() != null && user.getGrup().getRolesByGrup() != null) {
        if (roles != null) {
//            List<UserGrupRole> roles = user.getGrup().getRolesByGrup();
            for (Iterator<UserGrupRole> iterator = roles.iterator(); iterator.hasNext(); ) {
                UserGrupRole role = iterator.next();
                if (role.getRole() != null) {
                    if (role.getRole().getKode().equals(kode)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void log(String activity, String description) {
        log(activity, description,null);
    }

    public void log(String activity, String description, WorkflowableDokumen doc) {
        String workingon = doc != null ?
                String.format("working on doc %s doc. no. %s [%s] %s status: %s",
                        doc.getKodeTipeDokumen(),
                        doc.getNomor(),
                        doc.getNomorDokumen(),
                        StringUtil.fromDate(doc.getTanggal()),
                        doc.getWorkflow() != null && doc.getWorkflow().getStatus() != null ? doc.getWorkflow().getStatus().getNama() : "no workflow status information"
                )
                : "";
        logger.info(String.format("user [%s] is doing %s activity : %s ;%s", (user != null ? user.getUsername() : "no user information"), activity, description, workingon));
    }

    public UserUser getUser() {
        return user;
    }
}
