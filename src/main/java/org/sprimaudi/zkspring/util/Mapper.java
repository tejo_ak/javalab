package org.sprimaudi.zkspring.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 7/24/12
 * Time: 10:44 PM
 * To change this template use File | Settings | File Templates.
 */
@Scope(value = "prototype")
@Component(value = "mapper")
public class Mapper extends HashMap<String, Object> {
    private Logger logger = LoggerFactory.getLogger(Mapper.class);

    public Mapper map(String key, Object val) {
        this.put(key, val);
        return this;
    }

    public Mapper nmap(String key, Object val) {
        Mapper map = new Mapper();
        map.put(key, val);
        return map;
    }

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            beandestroyed++;
            int left = mappers.size() - 1;
            logger.debug(String.format("===================================" +
                    "=======this is mapper destroying %s destroyed %s left %s"
                    , this.getId(), beandestroyed, left));
            mappers.remove(this.getId());
        } catch (Exception e) {

            e.printStackTrace();
            logger.debug("destroy exception" + e.getMessage());
        } finally {
            super.finalize();
        }
    }

    public static List<String> mappers = new ArrayList<String>();

    public static boolean mapperExists(String id) {

        for (Iterator<String> iterator = mappers.iterator(); iterator.hasNext(); ) {
            String next = iterator.next();
            if (next.equals(id)) {
                return true;
            }
        }
        return false;

    }

    @PostConstruct
    public void init() {
        beanno++;
        logger.debug("================================" +
                "observing post construct methods of bean mapperr # " + beanno + " mappers size " + mappers.size());
        String id = UUID.randomUUID().toString() + "_mapper_#" + beanno;
        this.setId(id);
        mappers.add(id);

    }

    public static int beanno = 0;
    public static int beandestroyed = 0;
}
