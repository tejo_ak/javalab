package org.sprimaudi.zkspring.util;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: lenovo
 * Date: 2/5/13
 * Time: 4:35 AM
 * To change this template use File | Settings | File Templates.
 */
public interface Scroller<T> {
    public void scroll(T entity);

    public List<T> fetch(int offset, int size);

}
