package org.sprimaudi.zkutil.workflow;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 8/11/12
 * Time: 11:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class DocumentIdentity {
    private Long id;
    private String nomor;
    private Date tanggal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }
}
