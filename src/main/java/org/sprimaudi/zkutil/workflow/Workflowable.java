package org.sprimaudi.zkutil.workflow;

import org.sprimaudi.zkspring.entity.Workflow;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 8/31/12
 * Time: 9:48 AM
 * To change this template use File | Settings | File Templates.
 */
public interface Workflowable {

    public Workflow getWorkflow();

}
