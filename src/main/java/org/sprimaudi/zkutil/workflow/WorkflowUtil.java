package org.sprimaudi.zkutil.workflow;

import com.djbc.utilities.StringUtil;
import org.sprimaudi.exception.DocumentCheckedoutException;
import org.sprimaudi.exception.NonWorkflowDocumentException;
import org.sprimaudi.zkspring.entity.*;
import org.sprimaudi.zkspring.repository.ReferensiRepository;
import org.sprimaudi.zkspring.repository.WorkflowRepository;
import org.sprimaudi.zkspring.repository.WorkflowStatusRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 8/11/12
 * Time: 9:22 AM
 * To change this template use File | Settings | File Templates.
 */
@Component(value = "workflowUtil")
public abstract class WorkflowUtil<T extends WorkflowableDokumen> {
    @Inject
    WorkflowRepository workflowRepository;
    @Inject
    WorkflowStatusRepository workflowStatusRepository;
    @Inject
    ReferensiRepository referensiRepository;

    public final static Long GRUP_JENIS_WORKFLOW = 17L;
    private static final Long GRUP_KEPUTUSAN_WORKFLOW = 18L;
    private static final Long GRUP_STATS_WORKFLOW = 19L;

//    public abstract String getKodeJenisWorkflow();

    public Referensi getJenisWorkflow(T doc) {
        String kode = doc.getKodeTipeDokumen();
        if ("".equals(StringUtil.nvl(kode))) {
            throw new RuntimeException("Kode jenis workflow tidak ditemukan");
        }
        Referensi jenis = referensiRepository.byKodeGrup(kode, GRUP_JENIS_WORKFLOW);
        return jenis;

    }

    public void assignWorkflow(T dokumen, Workflow workflow) {
        String s = dokumen.getKodeTipeDokumen();
        if (s != null && !"".equals(s)) {
            if (workflow.getJenis() == null) {
                Referensi r = referensiRepository.byGrupAndKode(GRUP_JENIS_WORKFLOW, s);
                if (r != null) {
                    workflow.setJenis(r);
                    workflow = workflowRepository.save(workflow);
                }
            }

        }
    }
//
//    public abstract Workflow getWorkflow(T dokumen);
//
//    public abstract DocumentIdentity getDocumentIdentity(T document);

    public void release(T doc) {
        Workflow w = doc.getWorkflow();
        if (w == null) {
            w.setUser(null);
            workflowRepository.save(w);
        }
    }

    public void checkout(T doc, UserUser user) {
        if (doc == null || user == null) {
            return;
        }
        Workflow w = doc.getWorkflow();
        if (w == null) {
            throw new NonWorkflowDocumentException("This document has no workflow information set");
        }
        checkout(w, user);
    }

    public void checkout(Workflow workflow, UserUser user) {
        if (workflow == null || user == null) {
            return;
        }
        if (workflow.getUser() != null && !workflow.getUser().getId().equals(user.getId())) {
            throw new DocumentCheckedoutException("Document is checkout by other user");
        }
        workflow.setUser(user);
        workflowRepository.save(workflow);
    }

    public T createWorkflow(T dokumen,
                            String kodeStatus,
                            String kodeKeputusan,
                            String catatan) {
        if (dokumen == null) {
            return null;
        }

//        DocumentIdentity docId = getDocumentIdentity(dokumen);
        Workflow oldWf = dokumen.getWorkflow();
        Workflow newWf = new Workflow();
        newWf.setId(null);


        WorkflowStatus ws = workflowStatusRepository.findByJenisKode(dokumen.getKodeTipeDokumen(), kodeStatus);
        Referensi kep = referensiRepository.byKodeGrup(kodeKeputusan, GRUP_KEPUTUSAN_WORKFLOW);
//
        newWf.setNomor(dokumen.getNomor());
        newWf.setTanggal(dokumen.getTanggal());
        newWf.setDocId(dokumen.getId());
        newWf.setController(1);
        newWf.setCatatan(catatan);
        newWf.setStatus(ws);
        newWf.setKeputusan(kep);
        Referensi jenis = referensiRepository.byGrupAndKode(GRUP_JENIS_WORKFLOW, dokumen.getKodeTipeDokumen());
        newWf.setJenis(jenis);
        newWf = workflowRepository.save(newWf);
        assignWorkflow(dokumen, newWf);
        if (oldWf != null) {
            oldWf.setCurrent(newWf);
            oldWf.setController(0);
            oldWf = workflowRepository.save(oldWf);
            workflowRepository.updateCurrent(oldWf, newWf); // Doesn't work, need to take a look on transactional annotation
//            updateCurrent(oldWf, newWf);
        }
        return dokumen;
    }

    public void updateCurrent(Workflow old, Workflow newWf) {
        List<Workflow> wfOld = workflowRepository.findByCurrent(old.getId());
        List<Workflow> wfApdate = new ArrayList<Workflow>();
        if (wfOld != null) {
            for (Iterator<Workflow> iterator = wfOld.iterator(); iterator.hasNext(); ) {
                Workflow wf = iterator.next();
                wf.setCurrent(newWf);
                wfApdate.add(wf);
            }
        }
        workflowRepository.save(wfApdate);
    }

    public void deleteWorkflows(T doc) {
        Workflow w = doc.getWorkflow();
        //release current workflow
        assignWorkflow(doc, null);
        if (doc != null) {
            List<Workflow> wos = workflowRepository.findByCurrent(w.getId());
            wos.add(w);
            workflowRepository.delete(wos);
        }
    }
}
