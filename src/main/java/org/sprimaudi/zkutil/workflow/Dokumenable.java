package org.sprimaudi.zkutil.workflow;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 8/31/12
 * Time: 9:49 AM
 * To change this template use File | Settings | File Templates.
 */
public interface Dokumenable {
    public Date getTanggal();

    public String getNomor();

    public String getNomorDokumen();

    public String getId();

    public String getKodeTipeDokumen();
}
