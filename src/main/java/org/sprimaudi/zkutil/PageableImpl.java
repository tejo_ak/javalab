package org.sprimaudi.zkutil;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Created with IntelliJ IDEA.
 * User: lenovo
 * Date: 1/28/13
 * Time: 6:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageableImpl implements Pageable {
    int pageNumber;
    private int pageSize;
    private int offset;
    private Sort sort;

    @Override
    public int getPageNumber() {
        return this.pageNumber;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getPageSize() {
        return this.pageSize;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getOffset() {
        return this.offset;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Sort getSort() {
        return this.sort;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }
}
