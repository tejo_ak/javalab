package org.sprimaudi.zkutil;

import com.djbc.utilities.StringUtil;
import org.zkoss.zhtml.Br;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/31/12
 * Time: 3:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class ComponentUtil {
    private static <T extends Component> List<T> find(Class<T> clazz, Component parent, int depth, boolean exitOnFirst, boolean easyFind) {
        List<T> comps = new ArrayList<T>();
        int nextDepth = depth - 1;
        List<Component> children = parent.getChildren();
        for (Iterator<Component> iterator = children.iterator(); iterator.hasNext(); ) {
            Component child = iterator.next();
            if (child.getClass().getName().equals(clazz.getName())) {
                comps.add((T) child);
                if (exitOnFirst) {
                    return comps;
                }
                if (easyFind) {
                    nextDepth = 0;//
                }
            }
            if (nextDepth > 0) {
                List<T> chils = find(clazz, child, nextDepth, exitOnFirst, easyFind);
                comps.addAll(chils);
                if (chils.size() > 0 && exitOnFirst)
                    return comps;
            }
        }
        return comps;
    }

    public static <T extends Component> List<T> find(Class<T> clazz, String sclass, Component parent, int depth, boolean exitOnFirst, boolean easyFind) {
        List<T> comps = new ArrayList<T>();
        int nextDepth = depth - 1;
        List<Component> children = parent.getChildren();
        for (Iterator<Component> iterator = children.iterator(); iterator.hasNext(); ) {
            Component child = iterator.next();
            boolean found = false;
            if (child.getClass().getName().equals(clazz.getName())) {
                if (sclass != null && !"".equals(sclass)) {
                    if (child instanceof HtmlBasedComponent) {
                        HtmlBasedComponent hc = (HtmlBasedComponent) child;
                        if (hc.getSclass() != null && hc.getSclass().contains(sclass)) {
                            comps.add((T) child);
                            found = true;
                        }
                    }
                } else {
                    comps.add((T) child);
                    found = true;
                }

                if (exitOnFirst && found) {
                    return comps;
                }
                if (easyFind && found) {
                    nextDepth = 0;//
                }
            }
            if (nextDepth > 0) {
                List<T> chils = find(clazz, sclass, child, nextDepth, exitOnFirst, easyFind);
                comps.addAll(chils);
                if (chils.size() > 0 && exitOnFirst)
                    return comps;
            }
        }
        return comps;
    }

    public static <T extends Component> List<T> find(Class<T> clazz, Component parent, int depth) {
        return find(clazz, parent, depth, false, false);
    }

    public static <T extends Component> List<T> easyFind(Class<T> clazz, Component parent, int depth) {
        return find(clazz, parent, depth, false, true);
    }

    public static <T extends Component> T findFirst(Class<T> clazz, Component parent, int depth) {
        List<T> t = find(clazz, parent, depth, true, false);
        if (t.size() > 0)
            return t.get(0);
        return null;
    }

    public static <T extends Component> T findParent(Class<T> clazz, Component child) {
        Component p = child.getParent();
        if (p != null) {
            if (p.getClass().getName().equals(clazz.getName())) {
                return (T) p;
            } else {
                return findParent(clazz, p);
            }

        }
        return null;
    }

    public static void nomorDocListcellStyle(Listitem item, String uppperContent, String lowerContent) {
        Listcell lcNpa = new Listcell();
        lcNpa.setParent(item);
        Div dvNpa = new Div();
        dvNpa.setParent(lcNpa);
        Label lblNpaNomor = new Label(uppperContent);
        lblNpaNomor.setStyle("text-decoration:underline");
        lblNpaNomor.setParent(dvNpa);
        new Br().setParent(dvNpa);
        Label lblNpaTanggal = new Label(lowerContent);
        lblNpaTanggal.setStyle("color:grey");
        lblNpaTanggal.setParent(dvNpa);
    }

}
