package org.sprimaudi.zkutil;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.zkoss.zul.Paging;

/**
 * Created with IntelliJ IDEA.
 * User: lenovo
 * Date: 1/28/13
 * Time: 6:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class PagingUtil {
    public static Pageable fromPaging(Paging paging) {
        if (paging != null) {
            PageableImpl page = new PageableImpl();
            page.setPageNumber(paging.getActivePage());
            page.setPageSize(paging.getPageSize());
            int offs = paging.getActivePage() * paging.getPageSize();
            page.setOffset(offs);
            return page;
        }
        return null;
    }
}
