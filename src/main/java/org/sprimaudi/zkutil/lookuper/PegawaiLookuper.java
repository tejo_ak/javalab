package org.sprimaudi.zkutil.lookuper;

import com.djbc.utilities.StringUtil;
import org.sprimaudi.zkspring.entity.Pegawai;
import org.sprimaudi.zkspring.repository.PegawaiRepository;
import org.sprimaudi.zkutil.lookup.LookupColumn;
import org.sprimaudi.zkutil.lookup.LookupUtil;
import org.springframework.stereotype.Component;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 7/28/12
 * Time: 10:39 AM
 * To change this template use File | Settings | File Templates.
 */
@Component(value = "pegawaiLookuper")
public class PegawaiLookuper extends LookupUtil<Pegawai, String> {
    @Inject
    PegawaiRepository pegawaiRepository;

    @Override
    public void rendering(Listitem listitem, Pegawai pegawai, int i) throws Exception {
        new Listcell(StringUtil.nvl(pegawai.getNama())).setParent(listitem);
        new Listcell(StringUtil.nvl(pegawai.getNip())).setParent(listitem);
        new Listcell(StringUtil.nvl(
                (pegawai.getPangkat() != null) ? pegawai.getPangkat().getNama() : null)
        ).setParent(listitem);
        new Listcell(StringUtil.nvl(
                (pegawai.getJabatan() != null) ? pegawai.getJabatan().getNama() : null)
        ).setParent(listitem);
        new Listcell(StringUtil.nvl(
                (pegawai.getUnit() != null) ? pegawai.getUnit().getNama() : null)
        ).setParent(listitem);
        //To change body of implemented methods use File | Settings | File Templates.
    }


    @Override
    public String getDefaultWidth() {
        return "650px";
    }

    @Override
    public String getDisplayer(Pegawai data) {
        return data.getNama();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setValue(Textbox textbox, Pegawai value) {
        super.setValue(textbox, value);    //To change body of overridden methods use File | Settings | File Templates.
        setSatelite("nip", textbox, value != null ? value.getNip() : "");
        setSatelite("pangkat", textbox, value != null && value.getPangkat() != null ? value.getPangkat().getNama() : "");
        setSatelite("jabatan", textbox, value != null && value.getJabatan() != null ? value.getJabatan().getNama() : "");
    }

    @Override
    public Pegawai getById(String id) {
        return pegawaiRepository.findOne(id);  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getKey(Pegawai data) {
        return data.getId();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<Pegawai> getModel(String lookupParams) {
        return pegawaiRepository.lookupNipNama(lookupParams);  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public LookupColumn[] getColumns() {
        return new LookupColumn[]{
                new LookupColumn("Nama", "30%"),
                new LookupColumn("NIP", "15%"),
                new LookupColumn("Pangkat", "17.5%"),
                new LookupColumn("Jabatan", "17.5%"),
                new LookupColumn("Unit", "20%")
        };  //To change body of implemented methods use File | Settings | File Templates.
    }


}
