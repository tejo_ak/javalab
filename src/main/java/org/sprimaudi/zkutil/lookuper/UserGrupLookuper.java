package org.sprimaudi.zkutil.lookuper;

import com.djbc.utilities.StringUtil;
import org.sprimaudi.zkspring.entity.Pegawai;
import org.sprimaudi.zkspring.entity.UserGrup;
import org.sprimaudi.zkspring.repository.UserGrupRepository;
import org.sprimaudi.zkutil.lookup.LookupColumn;
import org.sprimaudi.zkutil.lookup.LookupUtil;
import org.springframework.stereotype.Component;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 7/28/12
 * Time: 10:39 AM
 * To change this template use File | Settings | File Templates.
 */
@Component(value = "userGrupLookuper")
public class UserGrupLookuper extends LookupUtil<UserGrup, String> {
    @Inject
    UserGrupRepository userGrupRepository;

    @Override
    public void rendering(Listitem listitem, UserGrup grup, int i) throws Exception {
        new Listcell(StringUtil.nvl(grup.getNama())).setParent(listitem);
        new Listcell(StringUtil.nvl(grup.getKode())).setParent(listitem);
        new Listcell(StringUtil.nvl(grup.getKeterangan())).setParent(listitem);
        listitem.setValue(grup);
        //To change body of implemented methods use File | Settings | File Templates.
    }


    @Override
    public String getDefaultWidth() {
        return "300px";
    }

    @Override
    public String getDisplayer(UserGrup data) {
        return data.getNama();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public UserGrup getById(String id) {
        return userGrupRepository.findOne(id);  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getKey(UserGrup data) {
        return data.getId();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<UserGrup> getModel(String lookupParams) {
        return userGrupRepository.lookupNamaKode(lookupParams);  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public LookupColumn[] getColumns() {
        return new LookupColumn[]{
                new LookupColumn("Nama", "150px"),
                new LookupColumn("Kode", "75px"),
                new LookupColumn("Keterangan", "200px")
        };  //To change body of implemented methods use File | Settings | File Templates.
    }


}
