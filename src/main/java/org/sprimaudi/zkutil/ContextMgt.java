package org.sprimaudi.zkutil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sprimaudi.zkspring.entity.WorkflowStatus;
import org.sprimaudi.zkspring.util.UserMgt;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.*;

import javax.inject.Inject;
import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 9/1/12
 * Time: 12:23 AM
 * To change this template use File | Settings | File Templates.
 */
@org.springframework.stereotype.Component("ctx")
public class ContextMgt {
    @Inject
    UserMgt userMgt;
    String menuItemHideMode = "disabled";
    String toolbarButtonHideMode = "visible";
    String buttonHideMode = "visible";
    public static final String DISABLED_HIDE_MODE = "disabled";
    public static final String VISIBILITY_HIDE_MODE = "visible";

    Logger logger = LoggerFactory.getLogger(ContextMgt.class);

    public boolean isOpenshift() {
        //CHECK ANY MARKER FILE
        String webhome = getWebHomePath();
        String sfwebhome = ".WEBHOME";
        String sfhome = ".HOME";
        File fwebhome = new File(webhome, sfhome);
        File fhome = new File(webhome, sfhome);
        if (fhome.exists()) {
            return false;
        } else if (fwebhome.exists()) {
            return true;
        } else if (checkHomeWritable()) {
            try {
                fhome.createNewFile();
            } catch (IOException e) {
                logger.error(String.format("errorwrite WEB-HOME marker: %s", e.getMessage()), e);
                throw new RuntimeException(String.format("errorwrite WEB-HOME marker: %s", e.getMessage()), e);
            }
            return false;
        } else {
            try {
                fwebhome.createNewFile();
            } catch (IOException e) {
                logger.error(String.format("errorwrite WEB-HOME marker: %s", e.getMessage()), e);
                throw new RuntimeException(String.format("errorwrite WEB-HOME marker: %s", e.getMessage()), e);
            }
            return true;
        }
    }

    public boolean checkHomeWritable() {
        String homeFolder = System.getProperty("user.home");
        File fcheck = new File(homeFolder, "SIGNAL_FCHECK");
        try {
            fcheck.createNewFile();
        } catch (IOException e) {
            //file fcheck is not writable
            return false;
        }
        return true;

    }

    public String getSafeHome() {
        if (isOpenshift()) {
            return getWebHomePath();
        } else {
            return System.getProperty("user.home");
        }
    }

    private String getWebHomePath() {
        return Executions.getCurrent().getSession().getWebApp().getRealPath("/home");
    }


    public void switchContext(Grid grid, WorkflowStatus status, String menu) {
        if (grid == null) {
            throw new RuntimeException("Grid has not been set ");
        }
        String theMenu = (String) grid.getAttribute("menu");
        String theRole = (String) grid.getAttribute("role");
        String theStatus = (String) grid.getAttribute("status");
        Boolean hide = cekContext(theRole, theStatus, theMenu, status, menu);

        List<Button> btns = ComponentUtil.find(Button.class, grid, 12);
        for (Iterator<Button> iterator = btns.iterator(); iterator.hasNext(); ) {
            Button btn = iterator.next();
            if (checkAutonomousSwitch(btn)) {
                //don't do anythink if this button has its own switch parameter
                continue;
            }
            if (btn.getAttribute("disabled") == null) {
                btn.setAttribute("disabled", btn.isDisabled());
            }
            if (!(Boolean) btn.getAttribute("disabled")) {
                btn.setDisabled(checkRowContext(hide, status, menu, btn));
            }
        }
        List<Combobox> cbss = ComponentUtil.find(Combobox.class, grid, 12);
        for (Iterator<Combobox> iterator = cbss.iterator(); iterator.hasNext(); ) {
            Combobox cbx = iterator.next();
            if (cbx.getAttribute("disabled") == null) {
                cbx.setAttribute("disabled", (cbx.isDisabled() || cbx.isReadonly()));
            }
            String style = cbx.getStyle();
            if (!(Boolean) cbx.getAttribute("disabled")) {
                boolean hidde = checkRowContext(hide, status, menu, cbx);
                cbx.setReadonly(hidde);
                cbx.setButtonVisible(!hidde);
                cbx.setAutocomplete(!hidde);
                if (style == null || !style.contains("background-color: white")) {
                    cbx.setStyle(style + " ;background-color: white");
                }
                if (style == null || !style.contains(";color:")) {
                    cbx.setStyle(style + " ;color: black");
                }
            } else {
                cbx.setDisabled(false);
                cbx.setReadonly(true);
                cbx.setButtonVisible(false);
                if (style == null || !style.contains("background-color: white")) {
                    cbx.setStyle(style + " ;background-color: white");
                }
                if (style == null || !style.contains(";color:")) {
                    cbx.setStyle(style + " ;color: black");
                }
            }
        }
        List<Toolbarbutton> tbtns = ComponentUtil.find(Toolbarbutton.class, grid, 12);
        for (Iterator<Toolbarbutton> iterator = tbtns.iterator(); iterator.hasNext(); ) {
            Toolbarbutton btn = iterator.next();
            boolean hidde = checkRowContext(hide, status, menu, btn);
            btn.setDisabled(hidde);
        }
        List<Datebox> dbs = ComponentUtil.find(Datebox.class, grid, 12);
        for (Iterator<Datebox> iterator = dbs.iterator(); iterator.hasNext(); ) {
            Datebox db = iterator.next();
            boolean hidde = checkRowContext(hide, status, menu, db);
            boolean isInactive = (db.isDisabled() || db.isReadonly());
            if (db.getAttribute("disabled") == null) {
                db.setAttribute("disabled", isInactive);
            }
            String style = db.getStyle();
            if (!(Boolean) db.getAttribute("disabled")) {
//                db.setDisabled(hide);
                db.setButtonVisible(!hidde);
                db.setReadonly(hidde);
                if (style == null || !style.contains("background-color: white")) {
                    db.setStyle(style + " ;background-color: white");
                }
                if (style == null || !style.contains(";color:")) {
                    db.setStyle(style + " ;color: black");
                }
            } else {
                db.setDisabled(false);
                db.setReadonly(true);
                db.setButtonVisible(false);
                if (style == null || !style.contains("background-color: white")) {
                    db.setStyle(style + " ;background-color: white");
                }
                if (style == null || !style.contains(";color:")) {
                    db.setStyle(style + " ;color: black");
                }
            }
        }
        List<Radio> rds = ComponentUtil.find(Radio.class, grid, 12);
        for (Iterator<Radio> iterator = rds.iterator(); iterator.hasNext(); ) {
            Radio rd = iterator.next();
            boolean hidde = checkRowContext(hide, status, menu, rd);
            rd.setDisabled(hidde);
        }
        List<Textbox> txts = ComponentUtil.find(Textbox.class, grid, 12);
        for (Iterator<Textbox> iterator = txts.iterator(); iterator.hasNext(); ) {
            Textbox txt = iterator.next();
            boolean isInactive = (txt.isDisabled() || txt.isReadonly());
            if (txt.getAttribute("disabled") == null) {
                txt.setAttribute("disabled", isInactive);
            }
            String style = txt.getStyle();
            if (!(Boolean) txt.getAttribute("disabled")) {
//                txt.setDisabled(hide);
                boolean hidde = checkRowContext(hide, status, menu, txt);
                txt.setReadonly(hidde);
                if (style == null || !style.contains("background-color: white")) {
                    txt.setStyle(style + " ;background-color: white");
                }
            } else {
                if (style == null || !style.contains("background-color: white")) {
                    txt.setStyle(style + " ;background-color: white");
                }
            }


        }
        List<Row> rows = ComponentUtil.find(Row.class, grid, 12);
        for (Iterator<Row> iterator = rows.iterator(); iterator.hasNext(); ) {
            Row r = iterator.next();
            boolean hidde = cekContext((String) r.getAttribute("visibility_role"),
                    (String) r.getAttribute("visibility_status"),
                    (String) r.getAttribute("visibility_menu"),
                    status,
                    menu);
            r.setVisible(!hidde);
        }

    }

    public boolean checkRowContext(boolean original, WorkflowStatus status, String menu, Component comp) {
        Row r = ComponentUtil.findParent(Row.class, comp);
        boolean needRowSwitch = r.getAttribute("role") != null ||
                r.getAttribute("status") != null ||
                r.getAttribute("menu") != null;
        if (needRowSwitch) {
            boolean rowEnability = cekContext((String) r.getAttribute("role"),
                    (String) r.getAttribute("status"),
                    (String) r.getAttribute("menu"),
                    status,
                    menu);
            return rowEnability;
        } else {
            return original;
        }
    }

    public boolean checkAutonomousSwitch(Button btn) {
        Component prnt = btn.getParent();
        if (prnt instanceof Hbox) {
            if ((prnt.getAttribute("menu") != null && !"".equals(prnt.getAttribute("menu")))
                    ||
                    (prnt.getAttribute("status") != null && !"".equals(prnt.getAttribute("status")))
                    ||
                    (prnt.getAttribute("role") != null && !"".equals(prnt.getAttribute("role")))
                    ) {
                return true;
            }
            if ((btn.getAttribute("menu") != null && !"".equals(btn.getAttribute("menu")))
                    ||
                    (btn.getAttribute("status") != null && !"".equals(btn.getAttribute("status")))
                    ||
                    (btn.getAttribute("role") != null && !"".equals(btn.getAttribute("role")))
                    ) {
                return true;
            }
        }
        return false;
    }

    public void switchContext(Menupopup popup, WorkflowStatus status, String menu) {
        List<Component> comps = Selectors.find(popup, "menuitem");
        for (Iterator<Component> iterator = comps.iterator(); iterator.hasNext(); ) {
            Component comp = (Component) iterator.next();
            if (comp instanceof Menuitem) {
                Menuitem mi = (Menuitem) comp;
                hide(mi, testToHideContext(mi, status, menu));
            }
        }
    }

    public void switchContext(Toolbar toolbar, WorkflowStatus status, String menu) {
        switchContext(toolbar, status, menu, false);
    }

    public boolean cekContext(String theRole, String theStatus, String theMenu, WorkflowStatus status, String menu) {
        boolean hide = true;
        String miRole = theRole;
        if (miRole == null || "".equals(miRole) || userMgt.hasRole(miRole)) {
            //role ok      cek workflow
            String statuses = theStatus;
            boolean statusOk = false;
            if (statuses != null) {
                String[] arrStatus = statuses.contains(",") ? statuses.split(",") : new String[]{statuses};
                for (int i = 0; i < arrStatus.length; i++) {
                    String state = arrStatus[i];
                    if (state.equals(status != null ? status.getKode() : "")) {
                        statusOk = true;
                        break;
                    }
                }
            }
            if (statusOk || statuses == null || status == null || "".equals(statuses)) {
                //status is OK
                boolean menuOk = false;
                String menus = (String) theMenu;
                if (menus != null) {
                    //check if menu only one or several
                    String[] arrMenu = menus.contains(",") ? menus.split(",") : new String[]{menus};
                    for (int i = 0; i < arrMenu.length; i++) {
                        String mn = arrMenu[i];
                        if (mn.equals(menu)) {
                            menuOk = true;
                            /// set visibility here
//                            break;
                            hide = false;
                            return hide;
                        }
                    }
                } else {
                    hide = false;
                }
            }

        }
        return hide;
    }

    public void initContext(Toolbar toolbar, String menu) {
        switchContext(toolbar, null, menu, true);
    }

    public void switchContext(Toolbar toolbar, WorkflowStatus status, String menu, boolean init) {
        List<Component> comps = Selectors.find(toolbar, "toolbarbutton");
        for (Iterator<Component> iterator = comps.iterator(); iterator.hasNext(); ) {
            Component comp = (Component) iterator.next();
            if (comp instanceof Toolbarbutton) {
                Toolbarbutton tb = (Toolbarbutton) comp;
                switchContext(tb, status, menu);
                hide(tb, testToHideContext(tb, status, menu, init));
            }
        }
    }

    public void switchContext(Row row, WorkflowStatus status, String menu) {
        String mode = (String) row.getAttribute("mode");
        boolean hide = testToHideContext(row, status, menu);
        if ("visibility".equals(mode)) {
            hide(row, hide);
        } else if ("editability".equals(mode)) {
            readOnly(row, hide);
        }
    }

    public void switchContext(Hbox row, WorkflowStatus status, String menu) {
        Integer depth = new Integer(row.getAttribute("depth") != null ? (String) row.getAttribute("depth") : "3");
        List<Button> btns = ComponentUtil.find(Button.class, row, depth);
        for (Iterator<Button> iterator = btns.iterator(); iterator.hasNext(); ) {
            Button btn = iterator.next();
            boolean hide = testToHideContext(btn, status, menu);
            btn.setVisible(!hide);
        }
    }

    public void hide(Toolbarbutton btn, boolean hide) {
        hide(btn, hide, toolbarButtonHideMode);
        btn.setDisabled(hide);
    }

    public void hide(Menuitem btn, boolean hide) {
        hide(btn, hide, menuItemHideMode);
        btn.setDisabled(hide);
    }

    public void hide(Row row, boolean hide) {
        row.setVisible(!hide);
    }

    public void readOnly(Row row, boolean hide) {
        Integer depth = new Integer(row.getAttribute("depth") != null ? (String) row.getAttribute("depth") : "3");
        List<Textbox> texts = ComponentUtil.find(Textbox.class, row, depth);
        for (Iterator<Textbox> iterator = texts.iterator(); iterator.hasNext(); ) {
            Textbox teks = iterator.next();
            teks.setReadonly(hide);
        }
        List<Radio> rds = ComponentUtil.find(Radio.class, row, depth);
        for (Iterator<Radio> iterator = rds.iterator(); iterator.hasNext(); ) {
            Radio teks = iterator.next();
            teks.setDisabled(hide);
        }
        List<Button> btns = ComponentUtil.find(Button.class, row, depth);
        for (Iterator<Button> iterator = btns.iterator(); iterator.hasNext(); ) {
            Button teks = iterator.next();
            teks.setDisabled(hide);
        }
        List<Datebox> dbs = ComponentUtil.find(Datebox.class, row, depth);
        for (Iterator<Datebox> iterator = dbs.iterator(); iterator.hasNext(); ) {
            Datebox teks = iterator.next();
            teks.setDisabled(hide);
        }

        List<Combobox> cbs = ComponentUtil.find(Combobox.class, row, depth);
        for (Iterator<Combobox> iterator = cbs.iterator(); iterator.hasNext(); ) {
            Combobox teks = iterator.next();
            teks.setReadonly(hide);
        }
    }

    public void hide(Button btn, boolean hide) {
        hide(btn, hide, toolbarButtonHideMode);
        btn.setDisabled(hide);
    }

    public void hide(Component mi, boolean hide, String mode) {
        if (DISABLED_HIDE_MODE.equals(mode)) {
            mi.setVisible(true);
        } else if (VISIBILITY_HIDE_MODE.equals(mode)) {
            mi.setVisible(!hide);
        }
    }

    public boolean testToHideContext(Component comp, WorkflowStatus status, String menu) {
        return testToHideContext(comp, status, menu, false);
    }

    public boolean testToHideContext(Component comp, WorkflowStatus status, String menu, boolean init) {
        boolean hide = true;
        String miRole = (String) comp.getAttribute("role");
        if (miRole == null || "".equals(miRole) || userMgt.hasRole(miRole)) {
            //role ok      cek workflow
            String statuses = (String) comp.getAttribute("status");
            boolean statusOk = false;
            if (statuses != null) {
                String[] arrStatus = statuses.contains(",") ? statuses.split(",") : new String[]{statuses};
                for (int i = 0; i < arrStatus.length; i++) {
                    String state = arrStatus[i];
                    if (state.equals(status != null ? status.getKode() : "")) {
                        statusOk = true;
                        break;
                    }
                    if ("NONE".equals(state) && status == null) {
                        statusOk = true;
                        break;
                    }
                }
            }
//            if (statusOk || statuses == null || status == null || "".equals(statuses)) {
            //Kalo pemeriksaan status OKE, atau attribut tag status tidak di set, maka pemeriksaan status dianggap Oke
            //Sehingga bisa lanjut ke pemeriksaan Menu
            if (statusOk || statuses == null) {
                //status is OK
                boolean menuOk = false;
                String menus = (String) comp.getAttribute("menu");
                if (menus != null) {
                    //check if menu only one or several
                    String[] arrMenu = menus.contains(",") ? menus.split(",") : new String[]{menus};
                    for (int i = 0; i < arrMenu.length; i++) {
                        String mn = arrMenu[i];
                        if (mn.equals(menu)) {
                            menuOk = true;
                            /// set visibility here

                            break;
                        }
                    }
                    if (menuOk) {
                        String sinit = (String) comp.getAttribute("init");
                        boolean binit = "true".equals(sinit);
                        if (init) {
                            hide = !binit;
                        } else {
                            hide = false;
                        }
                    }
                }
            }

        }
        return hide;
    }

    public String readProperty(String property, String devault) {
        return readProperty(property, devault, false);
    }

    public String readProperty(String property, String devault, boolean pleaseWrite) {

        //Check property file exists
        File propFile = new File(String.format("%s%sauditpro.properties", getSafeHome(), File.separator));
        Properties prop = new Properties();


        try {
            if (!propFile.exists()) {
                //initialize property file
                prop.setProperty("location", propFile.getAbsolutePath());
                prop.setProperty(property, devault);
                FileOutputStream fos = new FileOutputStream(propFile, false);
                prop.store(fos, "Property File of Auditpro is initialize");
                fos.close();
                return devault;
            } else {
                FileInputStream fis = new FileInputStream(propFile);
                prop.load(fis);
                fis.close();
                String sprop = prop.getProperty(property);
                if (sprop == null || "".equals(sprop) || pleaseWrite) {
                    prop.setProperty(property, devault);
                    FileOutputStream fos = new FileOutputStream(propFile, false);
                    prop.store(new FileOutputStream(propFile, false), "New Default Value of Set");
                    fos.close();
                    return devault;
                }
                return sprop;
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "";
    }


    public void setClientIp(String ip) {
        Executions.getCurrent().getDesktop().setAttribute("client_ip", ip);
    }

    public String getClientIp() {
        return (String) Executions.getCurrent().getDesktop().getAttribute("client_ip");
    }

    public void switchContext(Component mi, WorkflowStatus status, String menu) {
        boolean hide = true;
        String miRole = (String) mi.getAttribute("role");
        if (miRole == null || "".equals(miRole) || userMgt.hasRole(miRole)) {
            //role ok      cek workflow
            String statuses = (String) mi.getAttribute("status");
            boolean statusOk = false;
            if (statuses != null) {
                String[] arrStatus = statuses.contains(",") ? statuses.split(",") : new String[]{statuses};
                for (int i = 0; i < arrStatus.length; i++) {
                    String state = arrStatus[i];
                    if (state.equals(status != null ? status.getKode() : "")) {
                        statusOk = true;
                        break;
                    }
                }
            }
            if (statusOk || statuses == null || status == null || "".equals(statuses)) {
                //status is OK
                boolean menuOk = false;
                String menus = (String) mi.getAttribute("menu");
                if (menus != null) {
                    //check if menu only one or several
                    String[] arrMenu = menus.contains(",") ? menus.split(",") : new String[]{menus};
                    for (int i = 0; i < arrMenu.length; i++) {
                        String mn = arrMenu[i];
                        if (mn.equals(menu)) {
                            menuOk = true;
                            /// set visibility here
                            hide = false;
                            break;
                        }
                    }
                }
            }

        }
        boolean compDisabled = true;
        //set disability
        if (mi instanceof org.zkoss.zul.Button) {
            if (DISABLED_HIDE_MODE.equals(menuItemHideMode)) {
                mi.setVisible(true);
                compDisabled = hide;
            } else if (VISIBILITY_HIDE_MODE.equals(menuItemHideMode)) {
                mi.setVisible(!hide);
                compDisabled = false;
            }
            Button bt = (Button) mi;
            bt.setDisabled(compDisabled);
        }
        if (mi instanceof Menuitem) {
            if (DISABLED_HIDE_MODE.equals(menuItemHideMode)) {
                mi.setVisible(true);
                compDisabled = hide;
            } else if (VISIBILITY_HIDE_MODE.equals(menuItemHideMode)) {
                mi.setVisible(!hide);
                compDisabled = false;
            }
            Menuitem mit = (Menuitem) mi;
            mit.setDisabled(compDisabled);
        }
        if (mi instanceof Toolbarbutton) {
            if (DISABLED_HIDE_MODE.equals(toolbarButtonHideMode)) {
                mi.setVisible(true);
                compDisabled = hide;
            } else if (VISIBILITY_HIDE_MODE.equals(toolbarButtonHideMode)) {
                mi.setVisible(!hide);
                compDisabled = false;
            }
            Toolbarbutton tb = (Toolbarbutton) mi;
            tb.setDisabled(compDisabled);
        }
    }

}
