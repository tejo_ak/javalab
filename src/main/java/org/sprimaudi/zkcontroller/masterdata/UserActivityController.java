package org.sprimaudi.zkcontroller.masterdata;

import com.djbc.utilities.StringUtil;
import org.jote.util.dynaque.DynaqueResult;
import org.jote.util.dynaque.parametervalue.EqualParameterValue;
import org.jote.util.dynaque.parametervalue.RegularParameterValues;
import org.sprimaudi.zkspring.entity.UserActivity;
import org.sprimaudi.zkspring.repository.UserActivityRepository;
import org.sprimaudi.zkspring.service.ExcelService;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.sprimaudi.zkutil.PagingUtil;
import org.springframework.data.domain.Sort;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.io.ByteArrayOutputStream;
import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class UserActivityController extends SelectorComposer<Window> {

    public static final String zulpath = "zuls/masterdata/user_activity.zul";

    @Wire
    Textbox txtSearch;

    @WireVariable
    PageMgt pgm;

    @Wire("window")
    Window self;

    @Wire
    Listbox lstActivity;

    @Wire
    Paging pgActivity;


    @WireVariable
    UserActivityRepository userActivityRepository;


    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        lstActivity.setItemRenderer(new ListitemRenderer<UserActivity>() {
            @Override
            public void render(Listitem list, UserActivity o, int i) throws Exception {
                //To change body of implemented methods use File | Settings | File Templates.
                list.setValue(o);
                Listcell lc = new Listcell();
                lc.setParent(list);
                Vbox v = new Vbox();
                v.setParent(lc);
                Label l = new Label(o.getUser() != null ? o.getUser().getUsername() : "");
                l.setStyle("font-weight:bold");
                l.setParent(v);
                new Label(o.getDescription()).setParent(v);
                new Label("from " + StringUtil.nvl(o.getIp())).setParent(v);
                new Label("at " + StringUtil.fromSecond(o.getCreated())).setParent(v);
            }
        });

    }

    @Listen("onAfterCreate=window")
    public void onAfterCreate(Event evt) {
        Events.echoEvent("onOK", txtSearch, evt);
    }

    boolean fetchWithPagination = true;

    public List<UserActivity> fetchData() {
        Sort s = new Sort(new Sort.Order(Sort.Direction.DESC, "created"));

        DynaqueResult<UserActivity> dynaUa = userActivityRepository.dynamically(
                "user_activity_filter",
                s,
                fetchWithPagination ? PagingUtil.fromPaging(pgActivity) : null,
                new RegularParameterValues()
                        .add(EqualParameterValue.create("user", txtSearch.getText()))
                        .add(EqualParameterValue.create("grup", txtSearch.getText()))
                , null, null);
        List<UserActivity> uas = dynaUa.getContent();
        if (query) {
            Long l = userActivityRepository.countDynamically(dynaUa.getParameter()).getTotal();
            if (l != null) {
                pgActivity.setTotalSize(l.intValue());
            }
        }

        //reset switch
        query = false;
        fetchWithPagination = true;
        return uas;
    }

    public void prepareData() {
        List<UserActivity> uas = fetchData();
        lstActivity.setModel(new ListModelList<UserActivity>(uas, true));
    }

    boolean query = true;

    @Listen("onOK=#txtSearch")
    public void onSearch(Event evt) {
        query = true;
        fetchData();
        prepareData();
    }

    @WireVariable
    ExcelService excelService;

    @Listen("onClick=#tbExcel")
    public void onExcel(Event evt) {
        query = false;
        fetchWithPagination = false;
        List<UserActivity> uas = fetchData();
        String sfile = Executions.getCurrent().getSession().getWebApp().getRealPath("/resources/activity.xls");
        ByteArrayOutputStream boas = excelService.createUserActivity(sfile, uas);
        Filedownload.save(boas.toByteArray(), "application/ms-excel", "activitiy_data.xls");
    }

    @Listen("onPaging=#pgActivity")
    public void onPagingActivity(Event evt) {
        query = false;
        prepareData();
    }


    @WireVariable
    Mapper mapper;


    @WireVariable
    UserMgt usr;


}
