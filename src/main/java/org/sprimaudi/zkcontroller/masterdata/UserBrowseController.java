package org.sprimaudi.zkcontroller.masterdata;

import org.apache.commons.collections.CollectionUtils;
import org.jote.util.dynaque.DynaqueResult;
import org.jote.util.dynaque.parametervalue.LikeParameterValue;
import org.jote.util.dynaque.parametervalue.RegularParameterValues;
import org.sprimaudi.zkspring.entity.Pegawai;
import org.sprimaudi.zkspring.entity.UserGrup;
import org.sprimaudi.zkspring.entity.UserUser;
import org.sprimaudi.zkspring.repository.UserGrupRepository;
import org.sprimaudi.zkspring.repository.UserUserRepository;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.sprimaudi.zkutil.ContextMgt;
import org.sprimaudi.zkutil.PagingUtil;
import org.sprimaudi.zkutil.lookup.LookupWindow;
import org.sprimaudi.zkutil.lookuper.PegawaiLookuper;
import org.sprimaudi.zkutil.lookuper.UserGrupLookuper;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class UserBrowseController extends SelectorComposer<Window> {

    public static final String zulpath = "zuls/user/user_browse.zul";
    private String menu = "user_show";

    @WireVariable
    PageMgt pgm;

    @Wire
    Textbox txtPegawai, txtUsername, txtGrup, txtEmail, txtIdUser;

    @Wire
    Button btnSimpan;

    @Wire
    Hbox hbCrud;


    @Wire("window")
    Window self;

    @Wire
    Listbox lstUser, lstGrupRole, lstRole;

    @WireVariable
    UserGrupRepository userGrupRepository;

    @WireVariable
    UserUserRepository userUserRepository;

    @WireVariable
    PegawaiLookuper pegawaiLookuper;

    @WireVariable
    UserGrupLookuper userGrupLookuper;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        lstUser.setItemRenderer(new ListitemRenderer<UserUser>() {
            @Override
            public void render(Listitem listitem, UserUser o, int i) throws Exception {
                //To change body of implemented methods use File | Settings | File Templates.
                listitem.setValue(o);
                new Listcell(o.getUsername()).setParent(listitem);
                new Listcell(o.getGrup() != null ? o.getGrup().getNama() : "").setParent(listitem);
                new Listcell(o.getPegawai() != null ? o.getPegawai().getNip() : "").setParent(listitem);
                new Listcell(o.getPegawai() != null ? o.getPegawai().getNama() : "").setParent(listitem);

            }
        });

    }

    @WireVariable
    ContextMgt ctx;

    @Listen("onAfterCreate=window")
    public void onAfterCreate(Event evt) {
        prepareGrup();
        ctx.switchContext(hbCrud, null, menu);

    }

    public void prepareGrup() {
        Iterable<UserUser> users = userUserRepository.findAll();
        List<UserUser> ugs = new ArrayList<UserUser>();
        CollectionUtils.addAll(ugs, users.iterator());
        prepareUser(ugs);
    }

    public void prepareUser(List<UserUser> users) {
        ListModelList<UserUser> lmUg = new ListModelList<UserUser>(users);
        lstUser.setModel(lmUg);
    }

    @Listen("onClick=#btnGrup;onOK=#txtGrup")
    public void onGrup(Event evt) {
        LookupWindow<UserGrup> wug = userGrupLookuper.showLookup(txtGrup).doModal();
    }

    @Listen("onClick=#btnPegawai")
    public void onPegawai(Event evt) {
        LookupWindow<Pegawai> wpeg = pegawaiLookuper.showLookup(txtPegawai).doModal();
        if (wpeg.getSelected() != null) {
            txtEmail.setText(wpeg.getSelected().getEmail());
        }
    }

    @Listen("onClick=#btnSimpan")
    public void onSimpan(Event evt) {
        dataref = userUserRepository.save(extract());
        alert("Data Request Reference Entry has been saved successfully");
        menu = "dr_show";
        ctx.switchContext(hbCrud, null, menu);
        prepareData();
    }

    @Listen("onClick=#tbNewUser")
    public void onNewUser(Event evt) {
        dataref = new UserUser();
        show();
        txtPegawai.focus();
        menu = "user";
        ctx.switchContext(hbCrud, null, menu);
    }

    @Listen("onClick=#tbShowUser")
    public void onShowDr(Event evt) {
        dataref = lstUser.getSelectedItem() != null
                ? lstUser.getSelectedItem().getValue() != null
                ? (UserUser) lstUser.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            show();
            txtPegawai.focus();
            menu = "user_show";
            ctx.switchContext(hbCrud, null, menu);
        } else {
            alert("There is no any Data Request Row selected");
        }

    }

    @Listen("onClick=#tbEditUser")
    public void onEditDr(Event evt) {
        dataref = lstUser.getSelectedItem() != null
                ? lstUser.getSelectedItem().getValue() != null
                ? (UserUser) lstUser.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            show();
            txtPegawai.focus();
            menu = "user";
            ctx.switchContext(hbCrud, null, menu);
        } else {
            alert("There is no any Data Request Row selected");
        }

    }

    @Listen("onOK=#txtSearch")
    public void onSearch(Event evt) {
        prepareData();
    }

    @Listen("onUpdatePaging=#pgData")
    public void onUpdatePaging(Event evt) {
//        newQuery = false;
        Long l = userUserRepository.countDynamically(dresult.getParameter()).getTotal();
        pgData.setTotalSize(l != null ? l.intValue() : 0);
    }

    DynaqueResult dresult = null;
    public boolean newQuery = true;
    @Wire
    Paging pgData;
    @Wire
    Textbox txtSearch;

    @Listen("onPaging=#pgData")
    public void onPaging() {
        newQuery = false;
        prepareData();
    }

    public void prepareData() {
        if (newQuery) {
            pgData.setActivePage(0);
        }
        dresult = userUserRepository.dynamically(UserUserRepository.USER_FILTER, null, PagingUtil.fromPaging(pgData)
                , new RegularParameterValues()
                .add(LikeParameterValue.create("username", txtSearch.getText()))
                .add(LikeParameterValue.create("email", txtSearch.getText()))
                .add(LikeParameterValue.create("nama", txtSearch.getText()))
                .add(LikeParameterValue.create("grup", txtSearch.getText()))
                , null, null);
        prepareData(dresult.getContent());
        if (newQuery) {
            Events.echoEvent("onUpdatePaging", pgData, dresult);
        }
        newQuery = true;
    }

    @Listen("onSelect=#lstUser")
    public void onSelect(Event evt) {
        Events.echoEvent("onClick", self.getFellowIfAny("tbShowUser", true), evt);
    }

    public void prepareData(List<UserUser> grups) {
        ListModelList<UserUser> lmUg = new ListModelList<UserUser>(grups);
        lstUser.setModel(lmUg);
    }


    public UserUser extract() {
        UserUser pdr = txtIdUser.getText() != null
                ? !"".equals(txtIdUser.getText())
                ? userUserRepository.findOne(txtIdUser.getText())
                : new UserUser()
                : new UserUser();

        pdr.setUsername(txtUsername.getText());
        pdr.setGrup(userGrupLookuper.getValue(txtGrup));
        pdr.setPegawai(pegawaiLookuper.getValue(txtPegawai));
        pdr.setEmail(txtEmail.getText());
        pdr.setGrup(userGrupLookuper.getValue(txtGrup));
        return pdr;
    }

    private UserUser dataref;

    public void show() {
        if (dataref != null) {
            txtIdUser.setText(dataref.getId() != null ? "" + dataref.getId() : "");
            pegawaiLookuper.setValue(txtPegawai, dataref.getPegawai());
            userGrupLookuper.setValue(txtGrup, dataref.getGrup());
            txtEmail.setText(dataref.getEmail());
            txtUsername.setText(dataref.getUsername());
//            txtUsername.setText(dataref.getUserByGrup());
//            referensiUtil.toCombo(txtKategori, dataref.getKategori());
//            txtKeterangan.setText(dataref.getUraian());
//            txtKeterangan.setText(dataref.getTag());
        }
    }

    @WireVariable
    Mapper mapper;


    @WireVariable
    UserMgt usr;


}
