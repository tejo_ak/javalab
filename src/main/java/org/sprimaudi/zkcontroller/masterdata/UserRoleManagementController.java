package org.sprimaudi.zkcontroller.masterdata;

import org.jote.util.dynaque.DynaqueResult;
import org.jote.util.dynaque.parametervalue.LikeParameterValue;
import org.jote.util.dynaque.parametervalue.RegularParameterValues;
import org.sprimaudi.zkspring.entity.UserRole;
import org.sprimaudi.zkspring.repository.UserRoleRepository;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.sprimaudi.zkutil.ContextMgt;
import org.sprimaudi.zkutil.PagingUtil;
import org.sprimaudi.zkutil.ReferensiUtil;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class UserRoleManagementController extends SelectorComposer<Window> {

    public static final String zulpath = "zuls/user/user_role_management.zul";

    private String menu = "user_role_show";
    @Wire
    Textbox txtNama, txtKeterangan, txtKode, txtIdUserRole;

    @Wire
    Button btnSimpan;

    @Wire
    Hbox hbCrud;

    @WireVariable
    PageMgt pgm;

    @Wire("window")
    Window self;

    @Wire
    Listbox lstData;

    @Wire
    Checkbox cbAktif;

    @WireVariable
    UserRoleRepository userRoleRepository;

    @WireVariable
    ReferensiUtil referensiUtil;


    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        lstData.setItemRenderer(new ListitemRenderer<UserRole>() {
            @Override
            public void render(Listitem listitem, UserRole o, int i) throws Exception {
                //To change body of implemented methods use File | Settings | File Templates.
                listitem.setValue(o);
                new Listcell(o.getNama() != null ? o.getNama() : "").setParent(listitem);
                new Listcell(o.getKode() != null ? o.getKode() : "").setParent(listitem);
                new Listcell(o.getKeterangan() != null ? o.getKeterangan() : "").setParent(listitem);

            }
        });

    }

    @Listen("onAfterCreate=window")
    public void onAfterCreate(Event evt) {
        prepareData();
        ctx.switchContext(hbCrud, null, menu);
        ctx.switchContext(gdForm, null, menu);
    }

    @Listen("onSelect=#lstData")
    public void onSelect(Event evt) {
        Events.echoEvent("onClick", lstData.getFellowIfAny("tbShowDr", true), evt);
    }

    @Listen("onOK=#txtSearch")
    public void onSearch(Event evt) {
        prepareData();
    }

    public boolean newQuery = true;
    @Wire
    Paging pgData;
    @Wire
    Textbox txtSearch;

    @Listen("onPaging=#pgData")
    public void onPaging() {
        newQuery = false;
        prepareData();
    }

    @Listen("onUpdatePaging=#pgData")
    public void onUpdatePaging(Event evt) {
//        newQuery = false;
        Long l = userRoleRepository.countDynamically(dresult.getParameter()).getTotal();
        pgData.setTotalSize(l != null ? l.intValue() : 0);
    }

    DynaqueResult dresult = null;

    public void prepareData() {
        if (newQuery) {
            pgData.setActivePage(0);
        }
        dresult = userRoleRepository.dynamically(UserRoleRepository.USER_ROLE_FILTER, null, PagingUtil.fromPaging(pgData)
                , new RegularParameterValues()
                .add(LikeParameterValue.create("nama", txtSearch.getText()))
                .add(LikeParameterValue.create("keterangan", txtSearch.getText()))
                .add(LikeParameterValue.create("kode", txtSearch.getText()))
                , null, null);
        prepareData(dresult.getContent());
        if (newQuery) {
            Events.echoEvent("onUpdatePaging", pgData, dresult);
        }
        newQuery = true;

    }

    public void prepareData(List<UserRole> grups) {
        ListModelList<UserRole> lmUg = new ListModelList<UserRole>(grups);
        lstData.setModel(lmUg);
    }

    public UserRole extract() {
        UserRole tarif = txtIdUserRole.getText() != null
                ? !"".equals(txtIdUserRole.getText())
                ? userRoleRepository.findOne(txtIdUserRole.getText())
                : new UserRole()
                : new UserRole();
        tarif.setKeterangan(txtKeterangan.getText());
        tarif.setNama(txtNama.getValue());
        tarif.setKode(txtKode.getValue());
        return tarif;
    }

    private UserRole dataref;

    public void show() {
        if (dataref != null) {
            txtIdUserRole.setText(dataref.getId() != null ? "" + dataref.getId() : "");
            txtKeterangan.setText(dataref.getKeterangan());
            txtNama.setValue(dataref.getNama());
            txtKode.setValue(dataref.getKode());
//            cbAktif.setChecked("Y".equals(dataref.getAktif()));
        }
    }

    @WireVariable
    ContextMgt ctx;

    @Wire
    Grid gdForm;

    @Listen("onClick=#btnSimpan")
    public void onSimpan(Event evt) {
        dataref = userRoleRepository.save(extract());
        alert("Budget Tarif Reference Entry has been saved successfully");
        menu = "user_role_show";
        ctx.switchContext(hbCrud, null, menu);
        ctx.switchContext(gdForm, null, menu);
        prepareData();
    }

    @Listen("onClick=#tbNewDr")
    public void onNewDr(Event evt) {
        dataref = new UserRole();
        show();
        txtNama.focus();
        menu = "user_role";
        ctx.switchContext(hbCrud, null, menu);
        ctx.switchContext(gdForm, null, menu);
    }

    @Listen("onClick=#tbShowDr")
    public void onShowDr(Event evt) {
        dataref = lstData.getSelectedItem() != null ? lstData.getSelectedItem().getValue() != null
                ? (UserRole) lstData.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            show();
            txtNama.focus();
            menu = "user_role_show";
            ctx.switchContext(hbCrud, null, menu);
            ctx.switchContext(gdForm, null, menu);
        } else {
            alert("There is no any Data Request Row selected");
        }

    }

    @Listen("onClick=#tbEditDr")
    public void onEditDr(Event evt) {
        dataref = lstData.getSelectedItem() != null ? lstData.getSelectedItem().getValue() != null
                ? (UserRole) lstData.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            show();
            txtNama.focus();
            menu = "user_role";
            ctx.switchContext(hbCrud, null, menu);
            ctx.switchContext(gdForm, null, menu);
        } else {
            alert("There is no any Data Request Row selected");
        }

    }

    @Listen("onClick=#tbDeleteDr")
    public void onDeleteDr(Event evt) {
        dataref = lstData.getSelectedItem() != null ? lstData.getSelectedItem().getValue() != null
                ? (UserRole) lstData.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            if (Messagebox.show("Do you really want to delete this User Role Reference?", "Delete Tarif Information",
                    Messagebox.YES | Messagebox.NO,
                    Messagebox.QUESTION)
                    == Messagebox.YES) {
                userRoleRepository.delete(dataref);
                alert("selected User Role succesfully deleted");
                menu = "user_role";
                dataref = new UserRole();
                show();
                prepareData();
            }
        } else {
            alert("There is no any Data Request Row selected");
        }

    }


    @WireVariable
    Mapper mapper;


    @WireVariable
    UserMgt usr;


}
