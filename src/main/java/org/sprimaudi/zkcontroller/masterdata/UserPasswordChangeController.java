package org.sprimaudi.zkcontroller.masterdata;

import org.apache.commons.collections.CollectionUtils;
import org.sprimaudi.zkspring.entity.UserGrup;
import org.sprimaudi.zkspring.entity.UserGrupRole;
import org.sprimaudi.zkspring.entity.UserRole;
import org.sprimaudi.zkspring.entity.UserUser;
import org.sprimaudi.zkspring.repository.UserGrupRepository;
import org.sprimaudi.zkspring.repository.UserGrupRoleRepository;
import org.sprimaudi.zkspring.repository.UserRoleRepository;
import org.sprimaudi.zkspring.repository.UserUserRepository;
import org.sprimaudi.zkspring.service.MailService;
import org.sprimaudi.zkspring.service.UserService;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;
import sun.security.util.Password;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class UserPasswordChangeController extends SelectorComposer<Window> {

    public static final String zulpath = "zuls/user/user_password_change.zul";

    @Wire
    Textbox txtPassword, txtPasswordConfirm;
    @WireVariable
    PageMgt pgm;

    @Wire("window")
    Window self;

    @Wire
    Listbox lstGrup, lstGrupRole, lstRole;

    @WireVariable
    UserGrupRepository userGrupRepository;
    @WireVariable
    UserGrupRoleRepository userGrupRoleRepository;
    @WireVariable
    UserRoleRepository userRoleRepository;


    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.


    }

    @Listen("onAfterCreate=window")
    public void onAfterCreate(Event evt) {
    }


    @WireVariable
    Mapper mapper;

    @WireVariable
    UserService userService;

    @WireVariable
    UserUserRepository userUserRepository;

    @WireVariable
    UserMgt usr;

    @WireVariable
    MailService mailService;

    @Listen("onClick=#btnSimpan")
    public void onSimpan(Event evt) {
        if (txtPassword.getText() != null ? txtPassword.getText().equals(txtPasswordConfirm.getText()) : false) {
            UserUser u = usr.getUser();
            String pwd = txtPassword.getText();
            String digest = userService.digestPassword(pwd);
            u.setPassword(digest);
            userUserRepository.save(u);
            alert("Password change succeded");
            mailService.sendMail("noreply@signal.org", "tejo.ak@gmail.com", "password change", "your password hasbeen change");
        } else {
            alert("Password and it's confirmation is not equal!!");
        }
    }

}
