package org.sprimaudi.zkcontroller.masterdata;

import org.apache.commons.collections.CollectionUtils;
import org.sprimaudi.zkspring.entity.UserGrup;
import org.sprimaudi.zkspring.entity.UserGrupRole;
import org.sprimaudi.zkspring.entity.UserRole;
import org.sprimaudi.zkspring.repository.UserGrupRepository;
import org.sprimaudi.zkspring.repository.UserGrupRoleRepository;
import org.sprimaudi.zkspring.repository.UserRoleRepository;
import org.sprimaudi.zkspring.service.UserService;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class UserRtgController extends SelectorComposer<Window> {

    public static final String zulpath = "zuls/user/user_rtg.zul";


    @WireVariable
    PageMgt pgm;

    @Wire("window")
    Window self;

    @Wire
    Listbox lstGrup, lstGrupRole, lstRole;

    @WireVariable
    UserGrupRepository userGrupRepository;
    @WireVariable
    UserGrupRoleRepository userGrupRoleRepository;
    @WireVariable
    UserRoleRepository userRoleRepository;


    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        lstGrup.setItemRenderer(new ListitemRenderer<UserGrup>() {
            @Override
            public void render(Listitem listitem, UserGrup o, int i) throws Exception {
                //To change body of implemented methods use File | Settings | File Templates.
                listitem.setValue(o);
                new Listcell(o.getNama()).setParent(listitem);
                new Listcell(o.getKeterangan()).setParent(listitem);

            }
        });
        lstGrupRole.setItemRenderer(new ListitemRenderer<UserGrupRole>() {
            @Override
            public void render(Listitem listitem, UserGrupRole o, int i) throws Exception {
                //To change body of implemented methods use File | Settings | File Templates.
                listitem.setValue(o);
                new Listcell(o.getRole() != null ? o.getRole().getKode() : "").setParent(listitem);
                new Listcell(o.getRole() != null ? o.getRole().getKeterangan() : "").setParent(listitem);

            }
        });
        lstRole.setItemRenderer(new ListitemRenderer<UserRole>() {
            @Override
            public void render(Listitem listitem, UserRole o, int i) throws Exception {
                //To change body of implemented methods use File | Settings | File Templates.
                listitem.setValue(o);
                new Listcell(o.getKode()).setParent(listitem);
                new Listcell(o.getKeterangan()).setParent(listitem);

            }
        });

    }

    @Listen("onAfterCreate=window")
    public void onAfterCreate(Event evt) {
        prepareGrup();
    }

    public void prepareGrup() {
        Iterable<UserGrup> iugs = userGrupRepository.findAll();
        List<UserGrup> ugs = new ArrayList<UserGrup>();
        CollectionUtils.addAll(ugs, iugs.iterator());
        prepareGrup(ugs);
    }

    public void prepareGrup(List<UserGrup> grups) {
        ListModelList<UserGrup> lmUg = new ListModelList<UserGrup>(grups);
        lstGrup.setModel(lmUg);
    }

    public void prepareRoleList(UserGrup selectedGrup) {
        if (selectedGrup != null) {
            List<UserGrupRole> groles = userGrupRoleRepository.findRoles(selectedGrup);
            prepareGrupRole(groles);
            List<UserRole> roles = new ArrayList<UserRole>();
            if (groles.size() > 0) {
                roles = userRoleRepository.findAssigned(groles);
                roles = userRoleRepository.findExceptAssigned(roles);
            } else {
                CollectionUtils.addAll(roles, userRoleRepository.findAll().iterator());
            }
            prepareRole(roles);
        }
    }

    public void prepareRole(List<UserRole> grups) {
        ListModelList<UserRole> lmRl = new ListModelList<UserRole>(grups);
        lstRole.setModel(lmRl);
    }

    public void prepareGrupRole(List<UserGrupRole> grups) {
        ListModelList<UserGrupRole> lmRl = new ListModelList<UserGrupRole>(grups);
        lstGrupRole.setModel(lmRl);
    }

    @WireVariable
    Mapper mapper;

    @WireVariable
    UserService userService;

    @WireVariable
    UserMgt usr;

    @Listen("onSelect=#lstGrup")
    public void onSelect(Event evt) {
        UserGrup ug = lstGrup.getSelectedItem() != null ? lstGrup.getSelectedItem().getValue() != null ? (UserGrup) lstGrup.getSelectedItem().getValue() : null : null;
        if (ug != null) {
            prepareRoleList(ug);

        }
    }

    @Listen("onClick=#miRemove")
    public void onRemove(Event evt) {
        UserGrupRole ug = lstGrupRole.getSelectedItem() != null ? lstGrup.getSelectedItem().getValue() != null ? (UserGrupRole) lstGrupRole.getSelectedItem().getValue() : null : null;
        if (ug != null) {
            UserGrup grup = ug.getGrup();
            userService.unAssignRoleOfGrup(ug);
            if (grup != null) {
                prepareRoleList(grup);
            }
        }

    }

    @Listen("onClick=#miAssign")
    public void onAssign(Event evt) {
        UserGrup ug = lstGrup.getSelectedItem() != null ? lstGrup.getSelectedItem().getValue() != null ? (UserGrup) lstGrup.getSelectedItem().getValue() : null : null;
        UserRole ur = lstRole.getSelectedItem() != null ? lstRole.getSelectedItem().getValue() != null ? (UserRole) lstRole.getSelectedItem().getValue() : null : null;
        userService.assignRoleToGrup(ug, ur);
        prepareRoleList(ug);
    }

}
