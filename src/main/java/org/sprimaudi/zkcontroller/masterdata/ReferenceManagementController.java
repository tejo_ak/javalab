package org.sprimaudi.zkcontroller.masterdata;

import org.jote.util.dynaque.parametervalue.EqualParameterValue;
import org.jote.util.dynaque.parametervalue.LikeParameterValue;
import org.jote.util.dynaque.parametervalue.RegularParameterValues;
import org.sprimaudi.zkspring.entity.Referensi;
import org.sprimaudi.zkspring.repository.ReferensiRepository;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.sprimaudi.zkutil.ContextMgt;
import org.sprimaudi.zkutil.PagingUtil;
import org.sprimaudi.zkutil.ReferensiUtil;
import org.springframework.data.domain.Sort;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class ReferenceManagementController extends SelectorComposer<Window> {

    public static final String zulpath = "zuls/masterdata/reference_management.zul";

    private String menu = "reference_show";
    @Wire
    Textbox txtSubject, txtNama, txtKeterangan, txtIdReferensi, txtKode, txtGrupReferensi;

    @Wire
    Decimalbox txtNilai, txtNum;

    @Wire
    Button btnSimpan;

    @Wire
    Hbox hbCrud;
    @Wire
    Combobox txtGrup, txtParent;


    @WireVariable
    PageMgt pgm;

    @Wire("window")
    Window self;

    @Wire
    Listbox lstData;

    @Wire
    Checkbox cbAktif;

    @Wire
    Intbox txtUrut;

    @Wire
    Combobox txtTarget;

    @Wire
    Toolbarbutton tbShowDr;
    @WireVariable
    ReferensiRepository referensiRepository;

    @WireVariable
    ReferensiUtil referensiUtil;


    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        lstData.setItemRenderer(new ListitemRenderer<Referensi>() {
            @Override
            public void render(Listitem listitem, Referensi o, int i) throws Exception {
                //To change body of implemented methods use File | Settings | File Templates.
                listitem.setValue(o);
                new Listcell(o.getNama()).setParent(listitem);
                new Listcell(o.getKode()).setParent(listitem);
                new Listcell(o.getKeterangan()).setParent(listitem);
                new Listcell(o.getNilai() != null ? o.getNilai().toString() : "").setParent(listitem);
                new Listcell(o.getParent() != null ? o.getParent().getNama() : "").setParent(listitem);
                new Listcell(o.getAktif() == 1 ? "Y" : "N").setParent(listitem);

            }
        });

    }


    @Listen("onSelect=#txtGrup")
    public void onGrupSelect(Event evt) {
        prepareData();
        prepareParent();
    }

    @Listen("onSelect=#lstData")
    public void onListDataSelect(Event evt) {
        Events.echoEvent("onClick", tbShowDr, evt);
    }


    private void prepareParent() {
        Referensi grup = referensiUtil.fromCombo(txtGrup);
        if (grup.getParentGrup() == null) {
            txtParent.getChildren().clear();
        } else {
            txtParent.setAttribute("grup", "" + grup.getParentGrup().getNum());
            referensiUtil.fillCombo(txtParent);
        }
    }

    @Listen("onAfterCreate=window")
    public void onAfterCreate(Event evt) {
        referensiUtil.fillCombo(txtGrup);
        prepareData();
        ctx.switchContext(hbCrud, null, menu);
    }

    @Wire
    Paging pgData;
    @Wire
    Textbox txtSearch;

    public void prepareData() {
        Referensi grup = referensiUtil.fromCombo(txtGrup);
        Sort s = new Sort(Sort.Order.create(Sort.Direction.ASC, Arrays.asList(new String[]{"urut"})));
        Long lg = grup != null ? grup.getNum() : 0L;
        List<Referensi> ugs = referensiRepository.dynamically(ReferensiRepository.REFERENSI_FILTER, s, PagingUtil.fromPaging(pgData),
                new RegularParameterValues()
                        .add(EqualParameterValue.create("grup", lg))
                        .add(LikeParameterValue.create("nama", txtSearch.getText()))
                        .add(LikeParameterValue.create("kode", txtSearch.getText()))
                        .add(LikeParameterValue.create("keterangan", txtSearch.getText()))
                , null, null).getContent();

        prepareData(ugs);
    }

    @Listen("onOK=#txtSearch")
    public void onSearch(Event evt) {
        prepareData();
    }

    public void prepareData(List<Referensi> grups) {
        ListModelList<Referensi> lmUg = new ListModelList<Referensi>(grups);
        lstData.setModel(lmUg);
    }

    public Referensi extract() {
        Referensi ref = txtIdReferensi.getText() != null
                ? !"".equals(txtIdReferensi.getText())
                ? referensiRepository.findOne(txtIdReferensi.getText())
                : new Referensi()
                : new Referensi();
        ref.setNama(txtNama.getText());
        Referensi grup = referensiUtil.fromCombo(txtGrup);
        ref.setGrup(grup != null ? grup.getNum() : null);
        ref.setKeterangan(txtKeterangan.getText());
        ref.setKode(txtKode.getText());
        ref.setUrut(txtUrut.getValue());
        ref.setNilai(txtNilai.getValue() != null ? txtNilai.getValue().longValue() : null);
        ref.setNum(txtNilai.getValue() != null ? txtNum.getValue().longValue() : null);
        ref.setAktif(cbAktif.isChecked() ? 1 : 0);
        ref.setParent(referensiUtil.fromCombo(txtParent));
        return ref;
    }

    private Referensi dataref;


    public void show() {
        if (dataref != null) {
            txtIdReferensi.setText(dataref.getId() != null ? "" + dataref.getId() : "");
            txtNama.setText(dataref.getNama());
            Referensi grup = referensiUtil.fromCombo(txtGrup);
            txtGrupReferensi.setText(grup != null ? grup.getNama() : null);
            txtUrut.setValue(dataref.getUrut());
            txtKode.setText(dataref.getKode());
            txtKeterangan.setText(dataref.getKeterangan());
            txtNilai.setValue(dataref.getNilai() != null ? new BigDecimal(dataref.getNilai()) : null);
            txtNum.setValue(dataref.getNum() != null ? new BigDecimal(dataref.getNum()) : null);
            referensiUtil.toCombo(txtParent, dataref.getParent());
            cbAktif.setChecked(dataref.getAktif() == 1);
        }
    }

    @WireVariable
    ContextMgt ctx;

    @Listen("onClick=#btnSimpan")
    public void onSimpan(Event evt) {
        dataref = referensiRepository.save(extract());
        alert(" System Reference Entry has been saved successfully");
        menu = "budget_tarif_show";
        ctx.switchContext(hbCrud, null, menu);
        prepareData();
    }

    @Listen("onClick=#tbNewDr")
    public void onNewDr(Event evt) {
        dataref = new Referensi();
        show();
        txtNama.focus();
        menu = "reference";
        ctx.switchContext(hbCrud, null, menu);
    }

    @Listen("onClick=#tbShowDr")
    public void onShowDr(Event evt) {
        dataref = lstData.getSelectedItem() != null ? lstData.getSelectedItem().getValue() != null
                ? (Referensi) lstData.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            show();
            txtNama.focus();
            menu = "reference_show";
            ctx.switchContext(hbCrud, null, menu);
        } else {
            alert("There is no any Data Request Row selected");
        }

    }

    @Listen("onClick=#tbEditDr")
    public void onEditDr(Event evt) {
        dataref = lstData.getSelectedItem() != null ? lstData.getSelectedItem().getValue() != null
                ? (Referensi) lstData.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            show();
            txtNama.focus();
            menu = "reference";
            ctx.switchContext(hbCrud, null, menu);
        } else {
            alert("There is no any Data Request Row selected");
        }

    }


    @WireVariable
    Mapper mapper;


    @WireVariable
    UserMgt usr;


}
