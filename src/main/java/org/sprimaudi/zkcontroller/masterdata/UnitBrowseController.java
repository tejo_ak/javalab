package org.sprimaudi.zkcontroller.masterdata;

import org.jote.util.dynaque.parametervalue.RegularParameterValues;
import org.sprimaudi.zkspring.entity.Unit;
import org.sprimaudi.zkspring.repository.UnitRepository;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.sprimaudi.zkutil.ContextMgt;
import org.sprimaudi.zkutil.ReferensiUtil;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class UnitBrowseController extends SelectorComposer<Window> {

    public static final String zulpath = "zuls/masterdata/unit_management.zul";

    private String menu = "unit_show";
    @Wire
    Textbox txtNama, txtKode, txtIdUnit, txtAlamat;

    @Wire
    Grid gdForm;

    @Wire
    Decimalbox txtTarif;

    @Wire
    Button btnSimpan;

    @Wire
    Hbox hbCrud;


    @WireVariable
    PageMgt pgm;

    @Wire("window")
    Window self;

    @Wire
    Listbox lstData;

    @Wire
    Checkbox cbAktif;

    @Wire
    Toolbarbutton tbShowDr;


    @WireVariable
    UnitRepository unitRepository;

    @WireVariable
    ReferensiUtil referensiUtil;


    @Wire
    Row rowForm;

    @Wire
    Grid gdTemuanBaku;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        lstData.setItemRenderer(new ListitemRenderer<Unit>() {
            @Override
            public void render(Listitem listitem, Unit o, int i) throws Exception {
                //To change body of implemented methods use File | Settings | File Templates.
                listitem.setValue(o);
                new Listcell(o.getNama()).setParent(listitem);
                new Listcell(o.getKode()).setParent(listitem);
                new Listcell(o.getAlamat()).setParent(listitem);

            }
        });

    }

    @Listen("onAfterCreate=window")
    public void onAfterCreate(Event evt) {
        prepareData();
        ctx.switchContext(hbCrud, null, menu);
        ctx.switchContext(gdForm, null, menu);
    }

    public void prepareData() {

        List<Unit> ugs = unitRepository.dynamically(UnitRepository.UNIT_FILTER, null, null
                , new RegularParameterValues(), null, null).getContent();
        prepareData(ugs);
    }

    public void prepareData(List<Unit> grups) {
        ListModelList<Unit> lmUg = new ListModelList<Unit>(grups);
        lstData.setModel(lmUg);
    }

    public Unit extract() {
        Unit unit = txtIdUnit.getText() != null
                ? !"".equals(txtIdUnit.getText())
                ? unitRepository.findOne(txtIdUnit.getText())
                : new Unit()
                : new Unit();
        unit.setKode(txtKode.getText());
        unit.setNama(txtNama.getText());
        unit.setAlamat(txtAlamat.getText());
//        tarif.setKode(txtKode.getText());
        return unit;
    }

    private Unit dataref;

    public void show() {
        if (dataref != null) {

            txtIdUnit.setText(dataref.getId() != null ? "" + dataref.getId() : "");
            txtKode.setText(dataref.getKode());
            txtAlamat.setText(dataref.getAlamat());
            txtNama.setText(dataref.getNama());
        }
    }

    @WireVariable
    ContextMgt ctx;

    @Listen("onClick=#btnSimpan")
    public void onSimpan(Event evt) {
        dataref = unitRepository.save(extract());
        alert("Employee information has been saved successfully");
        show();
        menu = "unit_show";
        ctx.switchContext(hbCrud, null, menu);
        ctx.switchContext(gdForm, null, menu);
        prepareData();
    }

    @Listen("onClick=#tbNewDr")
    public void onNewDr(Event evt) {
        dataref = new Unit();
        show();
        txtNama.focus();
        menu = "unit";
        ctx.switchContext(hbCrud, null, menu);
        ctx.switchContext(gdForm, null, menu);
    }

    @Listen("onClick=#tbShowDr")
    public void onShowDr(Event evt) {
        dataref = lstData.getSelectedItem() != null ? lstData.getSelectedItem().getValue() != null
                ? (Unit) lstData.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            show();
            txtNama.focus();
            menu = "unit_show";
            ctx.switchContext(hbCrud, null, menu);
            ctx.switchContext(gdForm, null, menu);
        } else {
            alert("There is no any Data Request Row selected");
        }

    }

    @Listen("onClick=#tbEditDr")
    public void onEditDr(Event evt) {
        dataref = lstData.getSelectedItem() != null ? lstData.getSelectedItem().getValue() != null
                ? (Unit) lstData.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            show();
            txtNama.focus();
            menu = "unit";
            ctx.switchContext(hbCrud, null, menu);
            ctx.switchContext(gdForm, null, menu);
        } else {
            alert("There is no any Data Request Row selected");
        }

    }

    @Listen("onClick=#tbDeleteDr")
    public void onDeleteDr(Event evt) {
        dataref = lstData.getSelectedItem() != null ? lstData.getSelectedItem().getValue() != null
                ? (Unit) lstData.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            if (Messagebox.show("Do you really want to delete this Unit Informantion?", "Delete Employee Information",
                    Messagebox.YES | Messagebox.NO,
                    Messagebox.QUESTION)
                    == Messagebox.YES) {
                unitRepository.delete(dataref);
                alert("selected Unit succesfully deleted");
                menu = "pegawai_browse";
                dataref = new Unit();
                show();
                prepareData();
            }
        } else {
            alert("There is no any Unit Row selected");
        }

    }

    @Listen("onSelect=#lstData")
    public void onSelect(Event evt) {
        Events.echoEvent("onClick", tbShowDr, evt);
    }

    @WireVariable
    Mapper mapper;


    @WireVariable
    UserMgt usr;


}
