package org.sprimaudi.zkcontroller.masterdata;

import org.sprimaudi.zkspring.entity.Pegawai;
import org.sprimaudi.zkspring.entity.UserGrup;
import org.sprimaudi.zkspring.entity.UserUser;
import org.sprimaudi.zkspring.repository.*;
import org.sprimaudi.zkspring.service.MailService;
import org.sprimaudi.zkspring.service.UserService;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.sprimaudi.zkutil.ReferensiUtil;
import org.sprimaudi.zkutil.lookup.LookupWindow;
import org.sprimaudi.zkutil.lookuper.UserGrupLookuper;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;


/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class UserInviteController extends SelectorComposer<Window> {

    public static final String zulpath = "zuls/user/user_invite.zul";

    @Wire
    Textbox txtPassword, txtPegawai;
    @WireVariable
    PageMgt pgm;

    @Wire("window")
    Window self;

    @Wire
    Listbox lstGrup, lstGrupRole, lstRole;

    @WireVariable
    UserGrupRepository userGrupRepository;
    @WireVariable
    UserGrupRoleRepository userGrupRoleRepository;
    @WireVariable
    UserRoleRepository userRoleRepository;


    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.


    }

    @WireVariable
    PegawaiRepository pegawaiRepository;

    @Listen("onAfterCreate=window")
    public void onAfterCreate(Event evt) {
        String idPeg = pgm.eventParam(String.class, evt, "pegawai");
        if (idPeg != null) {
            peg = pegawaiRepository.findOne(idPeg);
            txtPegawai.setText(peg.getNama());
            txtPegawai.setReadonly(true);
            txtPegawai.setStyle("backgroun-color: white");
        }
    }


    @WireVariable
    Mapper mapper;

    @WireVariable
    UserService userService;

    @WireVariable
    UserUserRepository userUserRepository;

    @WireVariable
    UserMgt usr;

    @WireVariable
    MailService mailService;

    @WireVariable
    UserGrupLookuper userGrupLookuper;

    @Wire
    Textbox txtGrup;

    @WireVariable
    ReferensiUtil referensiUtil;

    private Pegawai peg = null;


    @Listen("onClick=#btnSend")
    public void onSend(Event evt) {
        UserGrup ug = userGrupLookuper.getValue(txtGrup);
        if (ug != null) {
            userService.sendActivation(peg, ug);
            alert("Activation Code for this pegawai has already been sent");
            self.detach();
        } else {
            alert("Any User grup Information must be provided");
        }
    }

    @Listen("onClick=#btnGrup;onOK=#txtGrup")
    public void onGrup(Event evt) {
        LookupWindow<UserGrup> wug = userGrupLookuper.showLookup(txtGrup).doModal();
    }

}
