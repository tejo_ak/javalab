package org.sprimaudi.zkcontroller.masterdata;

import org.jote.util.dynaque.DynaqueResult;
import org.jote.util.dynaque.parametervalue.LikeParameterValue;
import org.jote.util.dynaque.parametervalue.RegularParameterValues;
import org.sprimaudi.zkspring.entity.Pegawai;
import org.sprimaudi.zkspring.entity.Unit;
import org.sprimaudi.zkspring.entity.UserUser;
import org.sprimaudi.zkspring.repository.PegawaiRepository;
import org.sprimaudi.zkspring.repository.UserUserRepository;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.sprimaudi.zkutil.ContextMgt;
import org.sprimaudi.zkutil.PagingUtil;
import org.sprimaudi.zkutil.ReferensiUtil;
import org.sprimaudi.zkutil.lookup.LookupWindow;
import org.sprimaudi.zkutil.lookuper.UnitLookuper;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class PegawaiBrowseController extends SelectorComposer<Window> {

    public static final String zulpath = "zuls/masterdata/pegawai_management.zul";

    private String menu = "baku_show";
    @Wire
    Textbox txtNama, txtNip, txtUnit, txtIdPegawai, txtEmail, txtSearch;

    @Wire
    Combobox txtJabatan, txtPangkat;

    @Wire
    Grid gdForm;

    @Wire
    Decimalbox txtTarif;

    @Wire
    Button btnSimpan;

    @Wire
    Hbox hbCrud;


    @WireVariable
    PageMgt pgm;

    @Wire("window")
    Window self;

    @Wire
    Listbox lstData;

    @Wire
    Checkbox cbAktif;

    @Wire
    Toolbarbutton tbShowDr;

    @WireVariable
    UnitLookuper unitLookuper;


    @WireVariable
    PegawaiRepository pegawaiRepository;

    @WireVariable
    ReferensiUtil referensiUtil;

    @Wire
    Row rowForm;

    @Wire
    Grid gdTemuanBaku;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        lstData.setItemRenderer(new ListitemRenderer<Pegawai>() {
            @Override
            public void render(Listitem listitem, Pegawai o, int i) throws Exception {
                //To change body of implemented methods use File | Settings | File Templates.
                listitem.setValue(o);
                new Listcell(o.getNama()).setParent(listitem);
                new Listcell(o.getNip()).setParent(listitem);
                new Listcell(o.getJabatan() != null ? o.getJabatan().getNama() : "").setParent(listitem);
                new Listcell(o.getPangkat() != null ? o.getPangkat().getNama() : "").setParent(listitem);
                new Listcell(o.getUnit() != null ? o.getUnit().getNama() : "").setParent(listitem);
                new Listcell(o.getEmail()).setParent(listitem);

            }
        });

    }

    @Listen("onAfterCreate=window")
    public void onAfterCreate(Event evt) {
        referensiUtil.fillCombo(txtJabatan);
        referensiUtil.fillCombo(txtPangkat);
        prepareData();
        ctx.switchContext(hbCrud, null, menu);
        ctx.switchContext(gdForm, null, menu);
    }

    @Listen("onOK=#txtSearch")
    public void onSearch(Event evt) {
        prepareData();
    }

    public boolean newQuery = true;
    @Wire
    Paging pgData;

    @Listen("onPaging=#pgData")
    public void onPaging() {
        newQuery = false;
        prepareData();
    }

    @Listen("onUpdatePaging=#pgData")
    public void onUpdatePaging(Event evt) {
//        newQuery = false;
        Long l = pegawaiRepository.countDynamically(dresult.getParameter()).getTotal();
        pgData.setTotalSize(l != null ? l.intValue() : 0);
    }

    DynaqueResult dresult = null;

    public void prepareData() {
        if (newQuery) {
            pgData.setActivePage(0);
        }
        dresult = pegawaiRepository.dynamically(PegawaiRepository.PEGAWAI_FILTER, null, PagingUtil.fromPaging(pgData)
                , new RegularParameterValues()
                .add(LikeParameterValue.create("nama", txtSearch.getText()))
                .add(LikeParameterValue.create("email", txtSearch.getText()))
                .add(LikeParameterValue.create("nip", txtSearch.getText()))
                .add(LikeParameterValue.create("jabatan", txtSearch.getText()))
                , null, null);
        prepareData(dresult.getContent());
        if (newQuery) {
            Events.echoEvent("onUpdatePaging", pgData, dresult);
        }
        newQuery = true;

    }

    public void prepareData(List<Pegawai> grups) {
        ListModelList<Pegawai> lmUg = new ListModelList<Pegawai>(grups);
        lstData.setModel(lmUg);
    }

    public Pegawai extract() {
        Pegawai peg = txtIdPegawai.getText() != null
                ? !"".equals(txtIdPegawai.getText())
                ? pegawaiRepository.findOne(txtIdPegawai.getText())
                : new Pegawai()
                : new Pegawai();
        peg.setJabatan(referensiUtil.fromCombo(txtJabatan));
        peg.setPangkat(referensiUtil.fromCombo(txtPangkat));
        peg.setNip(txtNip.getText());
        peg.setNama(txtNama.getText());
        peg.setEmail(txtEmail.getText());
        peg.setUnit(unitLookuper.getValue(txtUnit));
//        tarif.setKode(txtKode.getText());
        return peg;
    }

    private Pegawai dataref;

    public void show() {
        if (dataref != null) {

            txtIdPegawai.setText(dataref.getId() != null ? "" + dataref.getId() : "");
            txtNip.setText(dataref.getNip());
            txtEmail.setText(dataref.getEmail());
            txtNama.setText(dataref.getNama());
            referensiUtil.toCombo(txtJabatan, dataref.getJabatan());
            referensiUtil.toCombo(txtPangkat, dataref.getPangkat());
            unitLookuper.setValue(txtUnit, dataref.getUnit());
        }
    }

    @WireVariable
    ContextMgt ctx;


    @Listen("onClick=#btnSimpan")
    public void onSimpan(Event evt) {
        dataref = pegawaiRepository.save(extract());
        alert("Employee information has been saved successfully");
        show();
        menu = "pegawai_show";
        ctx.switchContext(hbCrud, null, menu);
        ctx.switchContext(gdForm, null, menu);
        prepareData();
    }

    @Listen("onClick=#tbNewDr")
    public void onNewDr(Event evt) {
        dataref = new Pegawai();
        show();
        txtNama.focus();
        menu = "pegawai";
        ctx.switchContext(hbCrud, null, menu);
        ctx.switchContext(gdForm, null, menu);
    }

    @Listen("onClick=#btnUnit")
    public void btnUnit(Event evt) {
        LookupWindow<Unit> p = unitLookuper.showLookup(txtUnit);
        p.doModal();
    }

    @Listen("onClick=#tbShowDr")
    public void onShowDr(Event evt) {
        dataref = lstData.getSelectedItem() != null ? lstData.getSelectedItem().getValue() != null
                ? (Pegawai) lstData.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            show();
            txtNama.focus();
            menu = "pegawai_show";
            ctx.switchContext(hbCrud, null, menu);
            ctx.switchContext(gdForm, null, menu);
        } else {
            alert("There is no any Data Request Row selected");
        }

    }

    @Listen("onClick=#tbEditDr")
    public void onEditDr(Event evt) {
        dataref = lstData.getSelectedItem() != null ? lstData.getSelectedItem().getValue() != null
                ? (Pegawai) lstData.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            show();
            txtNama.focus();
            menu = "pegawai";
            ctx.switchContext(hbCrud, null, menu);
            ctx.switchContext(gdForm, null, menu);
        } else {
            alert("There is no any Data Request Row selected");
        }

    }

    @Listen("onClick=#tbDeleteDr")
    public void onDeleteDr(Event evt) {
        dataref = lstData.getSelectedItem() != null ? lstData.getSelectedItem().getValue() != null
                ? (Pegawai) lstData.getSelectedItem().getValue() : null : null;
        if (dataref != null) {
            if (Messagebox.show("Do you really want to delete this Employee Informantion?", "Delete Employee Information",
                    Messagebox.YES | Messagebox.NO,
                    Messagebox.QUESTION)
                    == Messagebox.YES) {
                pegawaiRepository.delete(dataref);
                alert("selected Employee succesfully deleted");
                menu = "pegawai_browse";
                dataref = new Pegawai();
                show();
                prepareData();
            }
        } else {
            alert("There is no any Employee Row selected");
        }

    }

    @Listen("onSelect=#lstData")
    public void onSelect(Event evt) {
        Events.echoEvent("onClick", tbShowDr, evt);
        Events.echoEvent("onCheckActivationSendable", self, evt);
    }

    @WireVariable
    UserUserRepository userUserRepository;
    @Wire
    Toolbarbutton tbSendActiovation;

    @Listen("onCheckActivationSendable=window")
    public void isActivationSendable(Event evt) {
        //activation only for those who doesn't have any user accout;
        Pegawai peg = lstData.getSelectedItem() != null ? (Pegawai) lstData.getSelectedItem().getValue() : null;
        if (peg != null) {
            List<UserUser> users = userUserRepository.getOfPegawai(peg.getId());
            tbSendActiovation.setVisible(!(users.size() > 0));
        }
    }

    @Listen("onClick=#tbSendActiovation")
    public void onSendActiovation(Event event) {
        pgm.showModal(UserInviteController.zulpath, mapper.nmap("pegawai", dataref.getId()));
    }

    @WireVariable
    Mapper mapper;


    @WireVariable
    UserMgt usr;


}
