package org.sprimaudi.zkcontroller.main;

import antlr.CppCodeGenerator;
import org.sprimaudi.zkspring.service.UserService;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.sprimaudi.zkutil.ComponentUtil;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 8/7/12
 * Time: 1:34 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class LoginController extends SelectorComposer<Window> {
    public static final String zulpath = "zuls/main/login.zul";

    private int maxTimer = 2;

    @Wire
    private Timer tmrAutoLogin;

    @Wire
    private Button btnLogin;

    @Wire
    private Window winLog;

    @Wire
    Label lblTimer;

    @Wire
    Textbox txtUsername, txtPassword;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        lblTimer = (Label) Selectors.find(winLog, "#lblTimer").get(0);
        txtPassword = (Textbox) Selectors.find(winLog, "#txtPassword").get(0);
        txtUsername = (Textbox) Selectors.find(winLog, "#txtUsername").get(0);
        btnLogin = (Button) Selectors.find(winLog, "#btnLogin").get(0);

        tmrAutoLogin.start();

    }


    @Listen("onTimer=#tmrAutoLogin")
    public void onTimerAutoLogin(Event evt) {
        if (maxTimer <= 0) {
            lblTimer.setVisible(false);
            autologin();
            tmrAutoLogin.stop();
        } else {
            if (maxTimer == 1) {
                txtUsername.setText("tejo.ak");
                txtPassword.setText("p4ssword");
            }
            lblTimer.setVisible(true);
            lblTimer.setValue(String.format("auto login as Superuser in %ss ...", maxTimer));
        }
        maxTimer--;
    }

    @WireVariable
    UserService userService;

    @WireVariable
    PageMgt pgm;
    @WireVariable
    UserMgt usr;

    @Listen("onClick=#winLog #btnLogin")
    public void onClick(Event evt) {
        boolean valid = userService.superLogin(txtUsername.getText(), txtPassword.getText());
        if (valid) {
//            pgm.showMainOnly(HomeController2.zulpath);
            usr.resolveLogin(txtUsername.getText());
        } else {
            lblTimer.setVisible(true);
            lblTimer.setStyle("font-color:red");
            lblTimer.setValue("username atau password tidak sesuai !!");
        }
    }

    private void autologin() {
        Events.echoEvent("onClick", btnLogin, "login");
    }
}
