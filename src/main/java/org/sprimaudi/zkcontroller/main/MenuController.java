package org.sprimaudi.zkcontroller.main;

import org.sprimaudi.zkcontroller.masterdata.*;
import org.sprimaudi.zkspring.service.UserService;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.sprimaudi.zkutil.ComponentUtil;
import org.sprimaudi.zkutil.ContextMgt;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Menubar;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Window;

import java.util.Iterator;
import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/17/12
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class MenuController extends SelectorComposer<Window> {

    public static final String zulpath = "zuls/main/menu.zul";
    @Wire
    Menuitem mnApproval;

    @Wire
    Menubar menu;

    @WireVariable
    PageMgt pgm;

    @Wire("window")
    Window self;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        Events.echoEvent("onSniffMenu", self, comp);

    }

    @Listen("onSniffMenu=window")
    public void sniffMenu() {
        List<Menuitem> mis = ComponentUtil.find(Menuitem.class, menu, 16);
        for (Iterator<Menuitem> iterator = mis.iterator(); iterator.hasNext(); ) {
            Menuitem mi = iterator.next();
            mi.addEventListener("onClick", new EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    Events.echoEvent("onInterceptMenu", self, event.getTarget());
                }
            });
        }
    }

    @WireVariable
    UserService userService;
    @WireVariable
    ContextMgt ctx;


    @Listen("onInterceptMenu=window")
    public void interceptMenu(Event evt) {
        Component c = (Component) evt.getData();
        usr.log("menu access", c.getId());
        userService.logActivity(usr.getUser().getUsername(), c.getId(), ctx.getClientIp(), "menu access");
    }

    @WireVariable
    Mapper mapper;


    @Listen("onCreate=window")
    public void onMenu() {
    }

    @Listen("onClick=#mnHome")
    public void onHome(Event evt) {
        pgm.showMainOnly(HomeController.zulpath);
    }

    @WireVariable
    UserMgt usr;

    @Listen("onClick=#menu_logout")
    public void onLogout(Event evt) {
        usr.logout();
        Executions.getCurrent().getSession().invalidate();
        Executions.getCurrent().sendRedirect("index.zul");


    }

    // =====================================================
    // PEMERIKSAAN SECTION
    // =======================================================



    //    ========================================================
//    USER MANAGEMENT SECTION
//    ========================================================
    @Listen("onClick=#menu_user_assign_rtg")
    public void onMenuUserRtg(Event evt) {
        pgm.showMainOnly(UserRtgController.zulpath,
                mapper.map("menu", "user_rtg"));
    }

    @Listen("onClick=#menu_user_browse")
    public void onMenuUserBrowse(Event evt) {
        pgm.showMainOnly(UserBrowseController.zulpath,
                mapper.map("menu", "user_browse"));
    }

    @Listen("onClick=#menu_user_grup_browse")
    public void onMenuUserGrupBrowse(Event evt) {
        pgm.showMainOnly(UserGrupManagementController.zulpath,
                mapper.map("menu", "user_grup_browse"));
    }

    @Listen("onClick=#menu_user_role_browse")
    public void onMenuUserRoleBrowse(Event evt) {
        pgm.showMainOnly(UserRoleManagementController.zulpath,
                mapper.map("menu", "user_role_browse"));
    }

    @Listen("onClick=#menu_user_activity")
    public void onMenuUserActivity(Event evt) {
        pgm.showNav(UserActivityController.zulpath,
                mapper.map("menu", "menu_user_activity"));
    }


    @Listen("onClick=#menu_user_password_change")
    public void onMenuUserChange(Event evt) {
        pgm.showMainOnly(UserPasswordChangeController.zulpath,
                mapper.map("menu", "user_change"));
    }


    @Listen("onClick=#menu_master_employee_management")
    public void onMenuMasterEmployManagement(Event evt) {
        pgm.showMainOnly(PegawaiBrowseController.zulpath,
                mapper.map("menu", "pegawai_browse"));
    }

    @Listen("onClick=#menu_master_unit_management")
    public void onMenuMasterUnitManagement(Event evt) {
        pgm.showMainOnly(UnitBrowseController.zulpath,
                mapper.map("menu", "unit_browse"));
    }


    @Listen("onClick=#menu_master_reference_management")
    public void onMenumasterReferenceManagement(Event evt) {
        pgm.showMainOnly(ReferenceManagementController.zulpath,
                mapper.map("menu", "reference_browse"));
    }


    @Listen("onAfterCreate=window")
    public void onAfterCreate() {


    }
}
