package org.sprimaudi.zkcontroller.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sprimaudi.zkspring.util.PageMgt;
import org.zkoss.spring.DelegatingVariableResolver;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.*;

/**
 * Created by IntelliJ IDEA.
 * User: jote
 * Date: 10/10/12
 * Time: 5:05 PM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class NavigationContoller extends SelectorComposer<Window> {
    public static final String zulpath = "zuls/main/navigation.zul";
    @Wire
    Include treeMenuInclude, navigationInclude;

    @Wire
    Tabpanel tpMenu, tpData;

    @Wire
    Tab tabNav;

    @Wire
    Tabbox tbNavigation;

    @WireVariable
    PageMgt pgm;

    @Listen("onAfterCreate=window")
    public void onAfterCreate(Event evt) {
//        pgm.broadcast("onMenuTree", new HashMap<String, Object>());
//        pgm.showInclude(treeMenuInclude, MenuTreeContoller.zulpath, new HashMap<String, Object>());
//        pgm.showInclude(treeMenuInclude, MonitoringDokumenController.zulpath, new HashMap<String, Object>());
    }



    @Listen("onToggleMenu=window")
    public void onMenuTree(Event evt) {
        tbNavigation.setSelectedPanel(tbNavigation.getSelectedPanel().equals(tpMenu) ? tpData : tpMenu);
    }

    Logger logger = LoggerFactory.getLogger(NavigationContoller.class);

    @Listen("onNavigationShow=window")
    public void onNavigationShow(Event evt) {
        String zulpath = pgm.eventParam(String.class, evt, "zulpath");
        logger.debug("how many time does it executed?");
        if (zulpath != null) {
            pgm.showInclude(navigationInclude, zulpath, pgm.forward(evt));
            Window win = pgm.findChildWindow(navigationInclude);
            if (win != null && win.getTitle() != null) {
                tabNav.setLabel(win.getTitle());
            }
            tbNavigation.setSelectedPanel(tpData);
        }

    }

    @Listen("onCloseNavigation=window")
    public void onCloseNavigation(Event evt) {
        tbNavigation.setSelectedPanel(tpMenu);
    }
}


