package org.sprimaudi.zkcontroller.main;

import org.sprimaudi.zkspring.entity.UserUser;
import org.sprimaudi.zkspring.repository.PegawaiRepository;
import org.sprimaudi.zkspring.repository.UserUserRepository;
import org.sprimaudi.zkspring.service.UserService;
import org.zkoss.spring.DelegatingVariableResolver;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 8/7/12
 * Time: 1:34 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class AktivasiController extends SelectorComposer<Window> {
    public static final String zulpath = "zuls/main/aktivasi.zul";
    @Wire
    Label lblMessage;

    @Wire
    Textbox txtAktivasi, txtUsername, txtPassword, txtPasswordAgain;

    @Wire
    Row rowActivationButton, rowUsername, rowPassword, rowPasswordAgain, rowLoginButton;

    @Wire
    Toolbarbutton tbAktivasi, tbRepassword;

    public void hideLogin(boolean hide) {
        rowLoginButton.setVisible(!hide);
        rowUsername.setVisible(!hide);
        rowPassword.setVisible(!hide);
        rowPasswordAgain.setVisible(!hide);
        rowActivationButton.setVisible(hide);
    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        hideLogin(true);
    }

    @WireVariable
    UserUserRepository userUserRepository;
    @WireVariable
    UserService userService;
    UserUser usrToActivate = null;

    @Listen("onClick=#tbAktivasi")
    public void onAktivasi(Event evt) {
        List<UserUser> usrs = userUserRepository.getAktivasi(txtAktivasi.getText());
        if (usrs != null && usrs.size() > 0) {
            usrToActivate = usrs.get(0);
            hideLogin(false);
            txtUsername.setText(usrToActivate.getUsername());
        } else {
            alert("invalid activation key was provided !!!");
        }
    }

    @Listen("onClick=#tbRepassword")
    public void onRepassword(Event evt) {
        String username = "";

        if (usrToActivate != null) {
            if (usrToActivate.getUsername() == null || "".equals(usrToActivate.getUsername())) {
                //user has not specified any username
                //check if this user name is exists
                if (txtUsername.getText() == null || "".equals(txtUsername)) {
                    alert("You have not fill in the username information");
                    return;
                }
                List<UserUser> usrs = userUserRepository.getLongin(txtUsername.getText());
                if (usrs != null && usrs.size() > 0) {
                    alert("the username you requested is already taken");
                    return;
                }
                usrToActivate.setUsername(txtUsername.getText());
            }
            if (txtPassword.getText() == null || "".equals(txtPassword.getText())) {
                alert("Please don't leave new password information blank");
                return;
            }
            if (!txtPassword.getText().equals(txtPasswordAgain.getText())) {
                alert("password entry and password confirmation is different");
                return;
            }
            String pwd = userService.digestPassword(txtPassword.getText());
            usrToActivate.setPassword(pwd);
            usrToActivate.setAktivasi(null);
            userUserRepository.save(usrToActivate);
            alert("your user information hasbeen successufully activated, please login");
            tbRepassword.setVisible(false);
            self.detach();
        }
    }

    @Wire("window")
    Window self;
}
