package org.sprimaudi.zkcontroller.main;

import org.sprimaudi.zkcontroller.lab.TvomController;
import org.sprimaudi.zkspring.repository.UserActivityRepository;
import org.sprimaudi.zkspring.repository.WorkflowRepository;
import org.sprimaudi.zkspring.service.AppInitService;
import org.sprimaudi.zkspring.service.CheatService;
import org.sprimaudi.zkspring.service.ReferensiService;
import org.sprimaudi.zkspring.service.UserService;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkutil.ContextMgt;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/15/12
 * Time: 6:32 PM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class IndexController extends SelectorComposer<Window> {
    @WireVariable
    PageMgt pgm;
    @WireVariable
    Mapper mapper;

    @Wire
    Label lblUsername, lblPassword;

    @Wire
    Textbox txtUsername, txtPassword;

    @Wire
    Toolbarbutton btnLogin;

    @Wire
    Row rowLogin;

    @Wire("window")
    Window self;


    @WireVariable
    ContextMgt ctx;

    String stage = "";

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        stage = ctx.readProperty("stage", "production");
        if ("development".equals(stage)) {
            txtUsername.setText("tejo.ak");
            txtPassword.setText("p4ssword");
            Events.postEvent("onClick", rowLogin, null);
            txtPassword.focus();
        }
    }


    @WireVariable
    AppInitService appInitService;

    @Listen("onAfterCreate=window")
    public void afterCreate() {


    }

    @WireVariable
    WorkflowRepository workflowRepository;
    @WireVariable
    ReferensiService referensiService;


    @Listen("onClick=#rowLogin")
    public void onRowLoginClick(Event evt) {
        txtUsername.setVisible(true);
        txtPassword.setVisible(true);
        btnLogin.setVisible(true);
        lblPassword.setVisible(false);
        lblUsername.setVisible(false);
        txtUsername.focus();

    }

    @Listen("onBlur=#rowLogin;onCancel=#rowLogin")
    public void onRowLoginBlur(Event evt) {
        txtUsername.setVisible(false);
        txtPassword.setVisible(false);
        btnLogin.setVisible(false);
        lblPassword.setVisible(true);
        lblUsername.setVisible(true);
//        txtUsername.focus();
    }

    @WireVariable
    UserService userService;

    @Listen("onOK=#txtUsername")
    public void onOkTxtUsername(Event evt) {
        txtPassword.focus();
    }

    @Listen("onOK=#txtPassword")
    public void onOkTxtpassword(Event evt) {
        Events.sendEvent("onClick", btnLogin, new HashMap());
    }

    @WireVariable
    UserActivityRepository userActivityRepository;

    @WireVariable
    CheatService cheatService;

    @Listen("onClick=#btnLogin")
    public void onBtnLoginClick(Event evt) {
        //Login Here
        String loginText = txtUsername.getText();
        if (cheatService.isCheat(loginText)) {
            cheatService.cheat(loginText);
            return;
        }
        boolean valid = userService.superLogin(txtUsername.getText(), txtPassword.getText());
        if (valid) {
            Desktop d = Executions.getCurrent().getDesktop();
            Executions.getCurrent().getSession().setAttribute("username", txtUsername.getText());
            Executions.getCurrent().getSession().setAttribute("password", txtPassword.getText());
            userService.logActivity(txtUsername.getText(), "User is logged in, with desktop id " + d.getId(), d.getSession().getRemoteHost(), "login");
//            try {
            if ("agung".equals(txtUsername.getText())) {
                Executions.getCurrent().sendRedirect(TvomController.zulpath);
            } else {
                Executions.getCurrent().sendRedirect(MainController.zulpath);
            }

//            } catch (IOException e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            }
        }

    }

    @Listen("onClick=#btnActivation")
    public void onActivation(Event evt) {
        //Login Here
        pgm.showModal(AktivasiController.zulpath,
                mapper.nmap("menu", "aktivasi"));
    }

    @Listen("onClick=#btnReset")
    public void onReset(Event evt) {
        //Login Here
        pgm.showModal(ResetPasswordController.zulpath,
                mapper.nmap("menu", "reset"));
    }
}



