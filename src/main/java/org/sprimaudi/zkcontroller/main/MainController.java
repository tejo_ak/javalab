package org.sprimaudi.zkcontroller.main;

import org.sprimaudi.zkspring.entity.UserUser;
import org.sprimaudi.zkspring.service.UserService;
import org.sprimaudi.zkspring.util.Mapper;
import org.sprimaudi.zkspring.util.PageMgt;
import org.sprimaudi.zkspring.util.UserMgt;
import org.sprimaudi.zkutil.ContextMgt;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.*;

import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * UserUser: UserUser
 * Date: 7/15/12
 * Time: 6:32 PM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class MainController extends SelectorComposer<Window> {
    public static final String zulpath = "petrol.zul";

    @WireVariable
    PageMgt pgm;
    @WireVariable
    Mapper mapper;

    @Wire("#mainWin > include")
    Include mainInclude;
    @Wire
    Include subInclude, navInclude, propInclude, menuInclude;
    @Wire
    South subPanel;
    @Wire
    East propPanel;
    @Wire
    West navPanel;
    @Wire
    Center mainPanel;
    @Wire("window")
    Window self;

    @Wire
    Label lblUserPredicate;
    @WireVariable
    UserMgt usr;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
        if (pgm != null) {
            pgm.initInclude(mainInclude, subInclude, navInclude, propInclude, menuInclude);
            pgm.initPanel(mainPanel, subPanel, navPanel, propPanel);
            pgm.initWindow(self);
        }
//        pgm.showMainOnly(LoginController.zulpath);\
        Desktop d = Executions.getCurrent().getDesktop();
        String uname = (String) Executions.getCurrent().getSession().getAttribute("username");
        String pwd = (String) Executions.getCurrent().getSession().getAttribute("password");

        boolean valid = userService.superLogin(uname, pwd);
        if (valid) {
//            pgm.showMainOnly(HomeController2.zulpath);
            usr.resolveLogin(uname);
        } else {
            Executions.getCurrent().sendRedirect("index.zul");
        }
    }

    @WireVariable
    UserService userService;



    @WireVariable
    ContextMgt ctx;

    @Listen("onAfterCreate=window")
    public void afterCreate() {
        ctx.       setClientIp(Executions.getCurrent().getSession().getRemoteHost());
//        eisService.calculateEisAnually();
    }

//    @Listen("onCtrlKey=window")
//    public void onCtrlKey(KeyEvent evt) {
//        //alert("this is ctrl key" + evt.getKeyCode());
//    }


    @Listen("onLoginResolved=window")
    public void onLoginResolved(Event evt) {

        UserUser user = pgm.eventParam(UserUser.class, evt, "user");
        if (user != null) {
            lblUserPredicate.setValue(String.format("%s - %s", (user.getPegawai() != null) ? user.getPegawai().getNama() : user.getUsername(), user.getGrup().getNama()));
            pgm.showMenu(MenuController.zulpath, mapper);
            pgm.showNavigation(NavigationContoller.zulpath, new HashMap<String, Object>());
        }
    }

    @Listen("onCtrlKey=window")
    public void keyPress(KeyEvent evt) {
        if (77 == evt.getKeyCode()) {
            pgm.broadcast("onToggleMenu", new HashMap<String, Object>());
        }
    }

    @Listen("onLoginLoggedOut=window")
    public void onLoginLoggedOut(Event evt) {


        lblUserPredicate.setValue(String.format("%s - %s", "Not loggedin", "not logged in"));
        pgm.showMenu("zuls/main/kosong.zul", mapper);
        pgm.showMainOnly(LoginController.zulpath, mapper);
    }
}



