package org.sprimaudi.zkcontroller.main;

import com.djbc.utilities.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sprimaudi.exception.UserEmailInvalidException;
import org.sprimaudi.zkspring.entity.Pegawai;
import org.sprimaudi.zkspring.entity.UserUser;
import org.sprimaudi.zkspring.repository.PegawaiRepository;
import org.sprimaudi.zkspring.repository.UserRoleRepository;
import org.sprimaudi.zkspring.repository.UserUserRepository;
import org.sprimaudi.zkspring.service.MailService;
import org.sprimaudi.zkspring.service.UserService;
import org.zkoss.spring.DelegatingVariableResolver;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import java.util.List;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * UserUser: jote
 * Date: 8/7/12
 * Time: 1:34 AM
 * To change this template use File | Settings | File Templates.
 */
@VariableResolver(DelegatingVariableResolver.class)
public class ResetPasswordController extends SelectorComposer<Window> {
    public static final String zulpath = "zuls/main/reset_password.zul";
    @Wire
    Label lblMessage;

    @Wire
    Textbox txtEmail;

    @WireVariable
    PegawaiRepository pegawaiRepository;

    @WireVariable
    UserUserRepository userUserRepository;

    @WireVariable
    UserService userService;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Listen("onClick=#tbReset")
    public void onReset(Event evt) {
        if (Messagebox.show("Are you sure want to reset your password?", "Send to leader approval",
                Messagebox.YES | Messagebox.NO,
                Messagebox.QUESTION)
                == Messagebox.YES) {
            reset();
        }

    }

    @WireVariable
    MailService mailService;
    private Logger logger = LoggerFactory.getLogger(ResetPasswordController.class);

    private void reset() {
        String email = txtEmail.getText();
        boolean found = false;
        try {
            userService.sendActivation(email);
            found = true;
        } catch (UserEmailInvalidException e) {
            logger.error(String.format("Error send activation code:", e.getMessage()), e);
            found = false;
        } catch (Exception e) {
            logger.error(String.format("Error send activation code:", e.getMessage()), e);
            throw new RuntimeException(String.format("Error send activation code:", e.getMessage()), e);
        }


        String msg = found ? "Your password hasbeen reset, and new activation code is sent to your email, please re activate your account by filling in the activation code sent." :
                "Your email doesn't match with any user information. You might not be a registered user";
        String style = found ? "font:10px verdana;color:gray" : "font:10px verdana;color:red";
        lblMessage.setValue(msg);
        lblMessage.setStyle(style);

    }

}
