/* select kode,nama,keterangan,nilai,num from referensi order by grup,id */
delete from lha;
delete from personal_auditee;
delete from budget;
delete from budget_header;
delete from temuan_item;
delete from temuan;
delete from anggota_tim;
delete from tembusan;
delete from surat_tugas;
delete from kuesioner;
delete from kuesioner_referensi;
delete from kuesioner_objektif;
delete from resiko_audit;
delete from permintaan_data_item;
delete from permintaan_data;
delete from permintaan_data_referensi;
delete from tujuan_audit;
delete from object_audit;
delete from droa;
delete from workflow;
delete from workflow_status;
delete from user_grup_role;
delete from user_user;
delete from user_role;
delete from user_grup;
delete from pegawai;
delete from unit;
delete from temuan_baku;
delete from temuan_isu;
delete from referensi where parent_grup is not null;
delete from referensi where parent is not null;
delete from referensi;

ALTER TABLE workflow AUTO_INCREMENT = 1;
ALTER TABLE workflow_status AUTO_INCREMENT = 1;
ALTER TABLE user_grup_role AUTO_INCREMENT = 1;
ALTER TABLE user_user AUTO_INCREMENT = 1;
ALTER TABLE user_role AUTO_INCREMENT = 1;
ALTER TABLE user_grup AUTO_INCREMENT = 1;
ALTER TABLE personal_auditee AUTO_INCREMENT = 1;
ALTER TABLE anggota_tim AUTO_INCREMENT = 1;
ALTER TABLE budget AUTO_INCREMENT = 1;
ALTER TABLE budget_header AUTO_INCREMENT = 1;
ALTER TABLE surat_tugas AUTO_INCREMENT = 1;
ALTER TABLE tembusan AUTO_INCREMENT = 1;
ALTER TABLE kuesioner AUTO_INCREMENT = 1;
ALTER TABLE kuesioner_referensi AUTO_INCREMENT = 1;
ALTER TABLE kuesioner_objektif AUTO_INCREMENT = 1;
ALTER TABLE tujuan_audit AUTO_INCREMENT = 1;
ALTER TABLE object_audit AUTO_INCREMENT = 1;
ALTER TABLE droa AUTO_INCREMENT = 1;
ALTER TABLE pegawai AUTO_INCREMENT = 1;
ALTER TABLE unit AUTO_INCREMENT = 1;
ALTER TABLE referensi AUTO_INCREMENT = 1;
ALTER TABLE resiko_audit AUTO_INCREMENT = 1;


INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (1, 1, 0, NULL, 'Grup GRUP', 'GP', 'GRUP', NULL, 1);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (2, 1, 1, NULL, 'GRUP Referensi Jenis \r\n\r\nAudit', NULL, NULL, NULL, 2);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (3, 1, 1, NULL, 'GRUP Referensi Jenis Object Auditee\r\n\r\nAudit', NULL, NULL, NULL, 3);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (4, 1, 1, NULL, 'Grup Incremental', 'ICM', 'Incremental', NULL, 4);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (5, 1, 1, NULL, 'Grup Status Droa', 'SDR', 'Status Droa', NULL, 5);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (6, 1, 1, NULL, 'Grup Status Object Audit', 'SOA', 'Status Object Audit', NULL, 6);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (7, 1, 1, NULL, 'Grup Pangkat', 'PKT', 'Grup Pangkat Pegawai', NULL, 7);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (8, 1, 1, NULL, 'Grup Jabatan', 'JBT', 'GrupJabatan Pegawai', NULL, 8);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (9, 1, 1, NULL, 'Grup Posisi Auditor', 'POA', 'Grup Posisi Auditor', NULL, 9);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (10, 1, 1, NULL, 'Grup Jenis Budget', 'POA', 'Grup Jenis Budget', NULL, 10);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (11, 1, 1, NULL, 'Grup Jenis Kuesioner', 'KUE', 'Grup Jenis Kuesioner', NULL, 11);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (12, 1, 1, NULL, 'Grup Kategori Kuesioner', 'KAK', 'Grup Kategori Kuesioner', NULL, 12);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (13, 1, 1, NULL, 'Grup Tingkat Risiko PTP', 'TRP', 'Grup Tingkat Risiko PTP', NULL, 13);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (14, 1, 1, NULL, 'Grup Status PTP', 'STP', 'Grup Status PTP', NULL, 14);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (15, 1, 1, NULL, 'Grup Kategori Risiko Temuan', 'RES', 'Grup Kategori Risiko Temuan', NULL, 15);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (16, 1, 1, NULL, 'Grup Kategori Fraud', 'FRD', 'Grup Kategori Fraud', NULL, 16);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (17, 1, 1, NULL, 'Grup Jenis Workflow', 'WKF', 'Grup Jenis Workflow', NULL, 17);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (18, 1, 1, NULL, 'Grup Keputusan Workflow', 'SWF', 'Grup Keputusan Workflow', NULL, 18);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (20, 1, 1, NULL, 'Grup Kategori Referensi Permintaan Data', 'PDT', 'Grup Kategori Permintaan Data', NULL, 20);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (88, 1, 1, NULL, 'Grup Target Audit', 'TGA', 'Grup Target Audit', NULL, 22);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (89, 1, 1, NULL, 'Grup Type Root Cause', 'RCT', 'Grup Type Root Cause', NULL, 23);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (21, 1, 1, NULL, 'Kategori Object Audit', 'KOA', 'Kategori Object Audit', NULL, 19);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (84, 1, 1, NULL, 'Grup Jenis Lampiran', 'LMP', 'Grup Jenis Lampiran', NULL, 21);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (22, 1, 2, NULL, 'Annual', 'AN', 'Annual', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (23, 1, 2, NULL, 'Khusus', 'KH', 'Khusus', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (24, 1, 3, NULL, 'Unit', 'UN', 'Unit', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (25, 1, 3, NULL, 'Personal', 'PN', 'Personal', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (26, 1, 4, NULL, 'Incremental Droa', 'IDR', 'Incremental Droa', 2012, 1);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (27, 1, 4, NULL, 'Incremental NPA', 'IPA', 'Incremental NPA', 2012, 1);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (28, 1, 4, NULL, 'Incremental ST', 'IST', 'Incremental ST', 2012, 1);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (29, 1, 4, NULL, 'Incremental Daftar Temuan', 'ITS', 'Incremental TS', 2012, 1);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (30, 1, 5, NULL, 'Draft Droa', 'DRF', 'Draft Droa', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (31, 1, 5, NULL, 'Penomoran', 'NMR', 'Penomoran', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (32, 1, 7, NULL, 'Pelaksana', 'PLK', 'Pelaksana', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (33, 1, 7, NULL, 'Manager', 'MGR', 'Manager', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (34, 1, 7, NULL, 'Direktur', 'DRK', 'Direktur', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (35, 1, 8, NULL, 'Kepala Cabang', 'KAC', 'Kepala Cabang', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (36, 1, 8, NULL, 'Staf', 'STF', 'Staf', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (37, 1, 9, NULL, 'Ketua', 'KTA', 'Ketua Tim', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (38, 1, 9, NULL, 'Anggota', 'AGT', 'Anggota', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (39, 1, 10, NULL, 'Transport', 'TRA', 'Transport', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (40, 1, 10, NULL, 'Akomodasi', 'ACO', 'Akomodasi', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (41, 1, 10, NULL, 'Harian', 'HAR', 'Harian', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (42, 1, 10, NULL, 'Stasionari', 'STA', 'Stasionari', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (43, 1, 11, NULL, 'Checklist', 'CLI', 'Checklist', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (44, 1, 12, NULL, 'Existence', 'EXT', 'Existence', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (45, 1, 12, NULL, 'Responsibility', 'RES', 'Responsibility', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (46, 1, 12, NULL, 'Likuidity', 'LIK', 'Likuidity', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (47, 1, 12, NULL, 'Kredibility', 'D9D5', 'Kredibility', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (48, 1, 13, NULL, 'High', 'HIG', 'High', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (49, 1, 13, NULL, 'Medium High', 'MEH', 'Medium High', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (50, 1, 13, NULL, 'Medium', 'MED', 'Medium', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (51, 1, 13, NULL, 'Medium Low', 'MEL', 'Medium Low', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (52, 1, 13, NULL, 'Low', 'LOW', 'Low', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (53, 1, 14, NULL, 'Open', 'OPN', 'Open', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (54, 1, 14, NULL, 'Close', 'CLS', 'Close', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (55, 1, 15, NULL, 'Personal', 'PER', 'Personal', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (56, 1, 15, NULL, 'Kredit', 'KRD', 'Kredit', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (57, 1, 15, NULL, 'Operasional', 'OPS', 'Operasional', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (58, 1, 15, NULL, 'Kepatuhan', 'PTH', 'Kepatuhan', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (59, 1, 15, NULL, 'Strategic', 'STG', 'Strategic', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (60, 1, 15, NULL, 'Legal', 'LGL', 'Legal', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (61, 1, 15, NULL, 'Reputasi', 'RPT', 'Reputasi', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (62, 1, 15, NULL, 'Pasar', 'PSR', 'Pasar', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (63, 1, 16, NULL, 'Internal Transaction Fraud', 'ITT', 'Internal Transaction Fraud', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (64, 1, 16, NULL, 'Internal Non Transaction Fraud', 'INT', 'Internal Non Transaction Fraud', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (65, 1, 16, NULL, 'External Transaction Fraud', 'ETT', 'External Transaction Fraud', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (66, 1, 16, NULL, 'External Non Transaction Fraud', 'ENT', 'External Transaction Fraud', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (67, 1, 16, NULL, 'Non Fraud', 'NFD', 'Non Fraud', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (68, 1, 17, NULL, 'Workflow Droa', 'DRA', 'DROA', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (69, 1, 17, NULL, 'Workflow Object Audit', 'WOA', 'Object Audit', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (70, 1, 17, NULL, 'Workflow Surat Tugas', 'WST', 'Surat Tugas', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (71, 1, 17, NULL, 'Workflow Temuan', 'WDT', 'Workflow Temuan', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (72, 1, 17, NULL, 'Workflow Tanggapan', 'WTG', 'Workflow Tanggapan', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (73, 1, 18, NULL, 'Keputusan Setuju', 'STJ', 'Setuju', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (74, 1, 18, NULL, 'Keputusan Tidak Setuju', 'TSJ', 'Tidak Setuju', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (75, 1, 18, NULL, 'Keputusan Revisi', 'RVS', 'Revisi', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (76, 1, 18, NULL, 'Penolakan', 'TLK', 'Ditolak', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (77, 1, 18, NULL, 'OK', 'OK', 'OK', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (78, 1, 19, NULL, 'Operasional', 'OPS', 'Operasional', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (79, 1, 19, NULL, 'IT', 'ITS', 'IT', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (80, 1, 19, NULL, 'Kredit', 'KDT', 'Kredit', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`, `parent`, `parent_grup`) VALUES (101, 1, 19, NULL, 'Umum', 'UMM', 'Umum', NULL, NULL, NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (81, 1, 20, NULL, 'Data Umum', 'DTU', 'Data Umum', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (82, 1, 20, NULL, 'Data Keuangan', 'DKU', 'Data Keuangan Umum', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (83, 1, 20, NULL, 'Data Kredit', 'DKT', 'Data Kredit', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (85, 1, 21, NULL, 'Surat Tugas', 'LST', 'Lampiran Surat Tugas', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (86, 1, 21, NULL, 'Kertas Kerja', 'LKK', 'Lampiran Kertas Kerja', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (87, 1, 21, NULL, 'Laporan Hasil Audit', 'LLA', 'Lampiran Laporan Hasil Audit', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (90, 1, 22, NULL, 'Data Center', 'DCT', 'Data Center', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (91, 1, 22, NULL, 'Deposito', 'DPS', 'Deposito', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (92, 1, 22, NULL, 'Giro', 'GRO', 'Giro', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (93, 1, 22, NULL, 'Kredit Usaha Rakyat', 'KUR', 'Kredit Usaha Rakyat', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (94, 1, 22, NULL, 'Internet Banking', 'IBN', 'Internet Banking', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (95, 1, 22, NULL, 'Kredit Perumahan', 'KPR', 'Kredit Perumahan', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (96, 1, 23, NULL, 'SDM', 'SDM', 'SDM', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (97, 1, 23, NULL, 'Teknologi', 'TGN', 'Teknologi', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (98, 1, 23, NULL, 'Prosedur', 'PSD', 'Prosedur', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (99, 1, 23, NULL, 'Eksternalitas', 'RLP', 'Eksternalitas', NULL, NULL);
INSERT INTO `referensi` (`id`, `aktif`, `grup`, `isi`, `keterangan`, `kode`, `nama`, `nilai`, `num`) VALUES (100, 1, 23, NULL, 'Lain lain', 'RAP', 'Lain lain', NULL, NULL);



INSERT INTO `kuesioner_objektif` (`created`, `keterangan`, `kode`, `objektif`, `kategori`) VALUES ('2012-08-24 16:24:44', NULL, '6469', 'Cek Existensi Perusahaan', 39);
INSERT INTO `kuesioner_objektif` (`created`, `keterangan`, `kode`, `objektif`, `kategori`) VALUES ('2012-08-24 16:24:59', NULL, '1D01', 'Cek Dokumen Pendirian', 39);
INSERT INTO `kuesioner_objektif` (`created`, `keterangan`, `kode`, `objektif`, `kategori`) VALUES ('2012-08-24 16:25:15', NULL, 'F70C', 'Cek Susunan Kepemimpinan', 39);
INSERT INTO `kuesioner_objektif` (`created`, `keterangan`, `kode`, `objektif`, `kategori`) VALUES ('2012-08-24 16:25:29', NULL, '76BD', 'Cek Kredibilitas Perusahaan', 40);

INSERT INTO `kuesioner_referensi` (`pertanyaan`, `jenis`, `objektif`) VALUES ('Apakah perusahaan memang benar benar ada', NULL, 1);
INSERT INTO `kuesioner_referensi` (`pertanyaan`, `jenis`, `objektif`) VALUES ('Apakah Alamat perusahaan sesuai pada akta pendirian', NULL, 1);
INSERT INTO `kuesioner_referensi` (`pertanyaan`, `jenis`, `objektif`) VALUES ('Apakah Bentuk fisik perusahaan telah sesuai dengan syarat syarat yang berlaku', NULL, 1);
INSERT INTO `kuesioner_referensi` (`pertanyaan`, `jenis`, `objektif`) VALUES ('Apakah dokumen pendirian perusahaan telah dimiliki dengan lengkap', NULL, 2);
INSERT INTO `kuesioner_referensi` (`pertanyaan`, `jenis`, `objektif`) VALUES ('Apakah Perusahaan telah terdaftar di deperteman perdpagangan', NULL, 2);
INSERT INTO `kuesioner_referensi` (`pertanyaan`, `jenis`, `objektif`) VALUES ('Apakah Perusahaan memiliki NPWP', NULL, 2);



INSERT INTO `unit` (`alamat`, `kode`, `nama`, `parent`) VALUES ('Jakarta', 'JKT', 'Cabang Rawamangun', NULL);
INSERT INTO `unit` (`alamat`, `kode`, `nama`, `parent`) VALUES ('Aceh', 'ACH', 'Cabang Aceh', NULL);
INSERT INTO `unit` (`alamat`, `kode`, `nama`, `parent`) VALUES ('Padang', 'PDG', 'Cabang Padang', NULL);
INSERT INTO `unit` (`alamat`, `kode`, `nama`, `parent`) VALUES ('Bandung', 'BDG', 'Cabang Bandung', NULL);
INSERT INTO `unit` (`alamat`, `kode`, `nama`, `parent`) VALUES ('Jogja', 'JGJ', 'Cabang Jogja', NULL);
INSERT INTO `unit` (`alamat`, `kode`, `nama`, `parent`) VALUES ('Semarang', 'SMG', 'Cabang Semarang', NULL);
INSERT INTO `unit` (`alamat`, `kode`, `nama`, `parent`) VALUES ('Surabaya', 'SBY', 'Cabang Surabaya', NULL);
INSERT INTO `unit` (`alamat`, `kode`, `nama`, `parent`) VALUES ('Trenggalek', 'TGK', 'Cabang Trenggalek', NULL);


INSERT INTO `pegawai` (`id`, `email`, `nama`, `nip`, `jabatan`, `pangkat`, `unit`) VALUES (1, 'tejo.ak@gmail.com', 'TEJO AK', '060114143', 36, 32, 1);
INSERT INTO `pegawai` (`id`, `email`, `nama`, `nip`, `jabatan`, `pangkat`, `unit`) VALUES (2, 'erwindombos@yahoo.com', 'ERWIN D', '060114144', 35, 33, 4);
INSERT INTO `pegawai` (`id`, `email`, `nama`, `nip`, `jabatan`, `pangkat`, `unit`) VALUES (3, 'basuki@yahoo.com', 'BASUKI SUJIWO', '060114145', 36, 33, 3);
INSERT INTO `pegawai` (`id`, `email`, `nama`, `nip`, `jabatan`, `pangkat`, `unit`) VALUES (4, 'rini@yahoo.com', 'RINI SUHARIANI', '060114146', 36, 34, 4);
INSERT INTO `pegawai` (`id`, `email`, `nama`, `nip`, `jabatan`, `pangkat`, `unit`) VALUES (5, 'raisa@gmail.com', 'RAISA MELLY', '060114147', 35, 32, 6);
INSERT INTO `pegawai` (`id`, `email`, `nama`, `nip`, `jabatan`, `pangkat`, `unit`) VALUES (6, 'gunawan@yahoo.com', 'GUNAWAN WICAK', '060114148', 36, 34, 5);
INSERT INTO `pegawai` (`id`, `email`, `nama`, `nip`, `jabatan`, `pangkat`, `unit`) VALUES (7, 'hadi@hotmail.com', 'HADI PURYADI', '060114149', 35, 32, 7);
INSERT INTO `pegawai` (`id`, `email`, `nama`, `nip`, `jabatan`, `pangkat`, `unit`) VALUES (8, 'dina@hotmail.com', 'DINA ASIH', '060114146', 36, 33, 8);


/*
============================================
insert grup user
*/

  INSERT INTO `user_grup` (`keterangan`, `kode`, `nama`) VALUES ('Grup Super User, All roles, no Crud', 'SU', 'Super User');
  INSERT INTO `user_grup` (`keterangan`, `kode`, `nama`) VALUES ('Auditor Staf', 'SA', 'Auditor Staff');
  INSERT INTO `user_grup` (`keterangan`, `kode`, `nama`) VALUES ('Direktur', 'DI', 'Direktur');
  INSERT INTO `user_grup` (`keterangan`, `kode`, `nama`) VALUES ('Mutu Audit', 'MA', 'Mutu Audit');

/*
============================================
insert role user
*/

INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (1, 'NDR', 'New Droa', 'Membuat Droa Baru');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (2, 'DDR', 'Save Draft Droa', 'Menyimpan Draft Droa');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (3, 'role_droa_check', 'Periksa Usulan Droa', 'Memeriksa Usulan Droa');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (4, 'role_droa_approve', 'Approval Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`,  `kode`, `nama`, `keterangan`) VALUES (5, 'role_droa_edit', 'Edit Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (6, 'role_droa_show', 'Show Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (7, 'role_droa_history', 'Show history Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (8, 'role_oa_history', 'Show history Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (9, 'role_oa_st', 'Show history Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`,  `kode`, `nama`, `keterangan`) VALUES (10, 'role_oa_data', 'Show history Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (11, 'role_oa_dts', 'Show history Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (12, 'role_oa_ptp', 'Show history Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (13, 'role_oa_ptp_closing', 'Show history Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (14, 'role_oa_ptp_asses', 'Show history Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`,   `kode`, `nama`, `keterangan`) VALUES (15, 'role_oa_lha', 'Show history Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`, `kode`, `nama`, `keterangan`) VALUES (16, 'role_oa_checklist', 'Show history Droa', 'Approval Droa');
INSERT INTO `user_role` (`id`, `keterangan`, `kode`, `nama`) VALUES (17, 'Approval Droa', 'role_dts_show', 'Show history Droa');
INSERT INTO `user_role` (`id`, `keterangan`, `kode`, `nama`) VALUES (18, 'Approval Droa', 'role_dts_edit', 'Show history Droa');
INSERT INTO `user_role` (`id`, `keterangan`, `kode`, `nama`) VALUES (19, 'Approval Droa', 'role_ptp_show', 'Show history Droa');
INSERT INTO `user_role` (`id`, `keterangan`, `kode`, `nama`) VALUES (20, 'Approval Droa', 'role_ptp_edit', 'Show history Droa');
INSERT INTO `user_role` (`id`, `keterangan`, `kode`, `nama`) VALUES (21, 'Approval Droa', 'role_oa_dts_response', 'Show history Droa');




/*
============================================
insert grup role
*/
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (1, 1, 1);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (2, 1, 2);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (3, 1, 3);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (4, 1, 4);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (5, 1, 5);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (6, 1, 6);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (7, 1, 7);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (8, 1, 8);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (9, 1, 9);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (10, 1, 10);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (11, 1, 11);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (12, 1, 12);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (13, 1, 13);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (14, 1, 14);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (15, 1, 15);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (16, 1, 16);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (17, 1, 17);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (18, 1, 18);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (19, 1, 19);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (20, 1, 20);
INSERT INTO `user_grup_role` (`id`, `grup`, `role`) VALUES (21, 1, 21);



INSERT INTO `user_user` (`password`, `username`, `grup`, `pegawai`) VALUES ('p4ssword', 'tejo.ak', 1, 1);
INSERT INTO `user_user` (`password`, `username`, `grup`, `pegawai`) VALUES ('p4ssword', 'erwin.dombos', 4, 2);




INSERT INTO `workflow_status` VALUES (1, NULL, 'DFT', 'Draft', 68);
INSERT INTO `workflow_status` VALUES (2, NULL, 'PRK', 'Pemeriksaan', 68);
INSERT INTO `workflow_status` VALUES (3, NULL, 'APP', 'Approval', 68);
INSERT INTO `workflow_status` VALUES (4, NULL, 'TLK', 'Penolakan', 68);
INSERT INTO `workflow_status` VALUES (5, NULL, 'NPA', 'Penomoran NPA', 68);
INSERT INTO `workflow_status` VALUES (6, NULL, 'DFT', 'Draft', 69);
INSERT INTO `workflow_status` VALUES (7, NULL, 'PLN', 'Planning', 69);
INSERT INTO `workflow_status` VALUES (8, NULL, 'NPA', 'Audit Program Issued', 69);
INSERT INTO `workflow_status` VALUES (9, NULL, 'ISU', 'Assigment Letter Issuance', 69);
INSERT INTO `workflow_status` VALUES (10, NULL, 'DTS', 'Execution', 69);
INSERT INTO `workflow_status` VALUES (11, NULL, 'LHA', 'Audit Reporting', 69);
INSERT INTO `workflow_status` VALUES (12, NULL, 'CPT', 'Completed', 69);
INSERT INTO `workflow_status` VALUES (37, NULL, 'LHC', 'Audit Report Checker', 69);
INSERT INTO `workflow_status` VALUES (38, NULL, 'LAP', 'Audit Report Approval', 69);
INSERT INTO `workflow_status` VALUES (39, NULL, 'LHR', 'Audit Reported', 69);
INSERT INTO `workflow_status` VALUES (30, NULL, 'PCN', 'Planned', 70);
INSERT INTO `workflow_status` VALUES (31, NULL, 'DFT', 'Draft', 70);
INSERT INTO `workflow_status` VALUES (32, NULL, 'CHK', 'Checker', 70);
INSERT INTO `workflow_status` VALUES (33, NULL, 'APR', 'Approval', 70);
INSERT INTO `workflow_status` VALUES (34, NULL, 'EXT', 'Extended', 70);
INSERT INTO `workflow_status` VALUES (35, NULL, 'ISD', 'Issued', 70);
INSERT INTO `workflow_status` VALUES (13, NULL, 'DFT', 'Draft', 71);
INSERT INTO `workflow_status` VALUES (14, NULL, 'APR', 'Approval', 71);
INSERT INTO `workflow_status` VALUES (15, NULL, 'TGP', 'Responding', 71);
INSERT INTO `workflow_status` VALUES (16, NULL, 'CHK', 'Checker', 71);
INSERT INTO `workflow_status` VALUES (17, NULL, 'CAP', 'Corective Action Plan', 71);
INSERT INTO `workflow_status` VALUES (18, NULL, 'TLK', 'Tolak', 71);
INSERT INTO `workflow_status` VALUES (19, NULL, 'RPA', 'Response Assesment', 71);
INSERT INTO `workflow_status` VALUES (20, NULL, 'DFT', 'Draft', 72);
INSERT INTO `workflow_status` VALUES (21, NULL, 'TGP', 'Responding', 72);
INSERT INTO `workflow_status` VALUES (22, NULL, 'RPC', 'Response Check', 72);
INSERT INTO `workflow_status` VALUES (23, NULL, 'RPA', 'Response Assesment', 72);
INSERT INTO `workflow_status` VALUES (24, NULL, 'NCD', 'Nonconformity Closed', 72);
INSERT INTO `workflow_status` VALUES (25, NULL, 'CAP', 'Corective Action Plan Issued', 72);
INSERT INTO `workflow_status` VALUES (26, NULL, 'APR', 'Temporary Nonconformity Approval', 72);
INSERT INTO `workflow_status` VALUES (27, NULL, 'DRP', 'Nonconformity Dropped', 72);
INSERT INTO `workflow_status` VALUES (28, NULL, 'CAC', 'Corrective Action Plan Check', 72);
INSERT INTO `workflow_status` VALUES (29, NULL, 'CAF', 'Corrective Action Plan Final', 72);
INSERT INTO `workflow_status` VALUES (36, NULL, 'CAD', 'Corrective Action Plan Draft', 72);


INSERT INTO `permintaan_data_referensi` (`id`, `created`, `subject`, `tag`, `uraian`, `kategori`) VALUES (1, '2012-09-22 12:28:26', 'Permintaan Data Inti', 'data penting sekalis', 'Data yang sangat penting untuk melakukan hal hal yang sangat penting juga', 81);
INSERT INTO `permintaan_data_referensi` (`id`, `created`, `subject`, `tag`, `uraian`, `kategori`) VALUES (2, '2012-09-26 16:36:27', 'Permintaan Data Kredit', 'kredit keuangan', 'Data Kredit meliputi \n1. Item penting,\n2. Item tidak penting\n3. Dan lain lain', 83);
INSERT INTO `permintaan_data_referensi` (`id`, `created`, `subject`, `tag`, `uraian`, `kategori`) VALUES (3, '2012-09-26 16:37:31', 'Data Keuangan Rutin', 'keuangan umum kertas kerja', 'Laporan Keuangan periodik bulanan, meliputi kertas kerja yang cukup lengkap', 82);